<?php

return [
    'routes' => [
        'reservaciones' => [
            'url' => '@/reservaciones@',
            'absolute' => false,
            'roles' => [
                'administracion', 'operaciones', 'reservaciones', 'director', 'sistemas'
            ]
        ],
        'reservaciones.editar' => [
            'url' => '@/reservaciones/(editar|reservar|store)@',
            'absolute' => true,
            'roles' => [
                'director', 'reservaciones', 'sistemas'
            ]
        ],
        'reportes' => [
            'url' => '@/reportes@',
            'absolute' => false,
            'roles' => [
                'operaciones', 'administracion', 'director', 'sistemas'
            ]
        ],
        'reportes.orden_de_servicio' => [
            'url' => '@/reportes/ordendesevicio@',
            'absolute' => false,
            'roles' => [
                'operaciones', 'director', 'sistemas'
            ]
        ],
        'reportes.bueno_para_cobro' => [
            'url' => '@/reportes/buenoparacobro@',
            'absolute' => false,
            'roles' => [
                'administracion', 'director', 'sistemas'
            ]
        ],
        'adminstracion' => [
            'url' => '@/agencia/(tipocambio|precio)@',
            'absolute' => false,
            'roles' => [
                'administrativo', 'director', 'sistemas'
            ]
        ]

    ]
];