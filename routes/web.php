<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Rutas de Voyager, deben ir primero
Voyager::routes();

// Reservaciones - UNUSED
Route::get('/reservaciones', ['as' => "reservaciones", "uses" => 'ReservacionesController@index']);

// Peticiones REST para autocomplementado
Route::get('/getreps/{id}', 'ReservacionesController@reps');
Route::get('/gettours/{id}', 'ReservacionesController@tours');
Route::get('/getpaquetetours/{id}', 'ReservacionesController@paquetestours');
Route::get('/gethoteles/{id}', 'ReservacionesController@hoteles');
Route::get('/getregiontour/{id}', 'ReservacionesController@regiontour');
Route::get('/getareahoteles/{id}', 'ReservacionesController@areahoteles');
Route::get('/getpickupshoteles/{id}/{area}', 'ReservacionesController@pickupshoteles');
Route::get('/getidiomastours/{id}', 'ReservacionesController@idiomastours');
Route::get('/getcuponesbyidreservacion/{id}', 'ReservacionesController@getcuponesByIdReservacion');


// Vista de reservacion
Route::get('/reservaciones/reservar', ['uses' => 'ReservacionesController@create', 'as' => 'reservaciones.create']);
// Guardar reservación
Route::post('/reservaciones/store', ['uses' => 'ReservacionesController@store']);

Route::get('/reservaciones/{id}/editar', ['uses' => 'ReservacionesController@edit', 'as' => 'reservaciones.edit']);

Route::post('/reservaciones/{id}/update', ['uses' => 'ReservacionesController@update']);

Route::get('/reservaciones/{id}/goshow', ['uses' => 'ReservacionesController@goShow', 'as' => 'reservaciones.goshow']);

Route::get('/reservaciones/{id}/noshow', ['uses' => 'ReservacionesController@noShow', 'as' => 'reservaciones.noshow']);

Route::get('/reservaciones/{id}/efectivo', ['uses' => 'ReservacionesController@efectivo', 'as' => 'reservaciones.efectivo']);

Route::post('/reservaciones/{id}/cancelar', ['uses' => 'ReservacionesController@cancelar', 'as' => 'reservaciones.cancelar']);

Route::post('/reservaciones/{id}/reagendar', ['uses' => 'ReservacionesController@reagendar', 'as' => 'reservaciones.reagendar']);

Route::get('/reservaciones/{id}', ['uses' => 'ReservacionesController@show', 'as' => 'reservaciones.show'])->where('id', '[0-9]+');

Route::get('/reservaciones/buscar', ['uses' => 'ReservacionesController@parametros', 'as' => 'reservaciones.parametros']);

Route::post('/reservaciones/resultados', ['uses' => 'ReservacionesController@resultados', 'as' => 'reservaciones.resultados']);

// Resultados de Busqueda en Excel
Route::post('/reservaciones/resultados/excel', 'BusquedaExporterController@exportExcelFile');


//REPORTES
//Route::get('/reservaciones/{reservas}export_excel', 'ExportExcelController@reporteExcelReservaciones');
Route::get('/reportes', ['uses' => 'ReportesController@reportes', 'as' => 'reportes']);

//ORDEN DE SERVICIO
//BUSCAR
Route::get('/reportes/ordendeservicio/buscar', ['uses' => 'ReportesController@buscarOrdenDeServicio', 'as' => 'ordendeservicio']);
//RESULTADOS
Route::post('/reportes/ordendeservicio/resultados', ['uses' => 'ReportesController@resultadosOrdenDeServicio', 'as' => 'ordendeservicio.resultados']);
//DESCARGA DE EXCEL
Route::get('/reportes/ordendeservicio/descargarExcel/{parametros?}', 'ExportExcelController@descargarExcelOrdenServicio');
// Orden de Servicio en Excel
Route::get('/reportes/ordendeservicio/excel', 'OrdenServicioExporterController@exportExcelFileFromTemplate');

//BUENO PARA COBRO
//BUSCAR
Route::get('/reportes/buenoparacobro/buscar', ['uses' => 'ReportesController@buscarBuenoParaCobro', 'as' => 'buenoparacobro']);
//RESULTADOS
Route::post('/reportes/buenoparacobro/resultados', ['uses' => 'ReportesController@resultadosBuenoParaCobro', 'as' => 'buenoparacobro.resultados']);
// Bueno para Cobro en Excel
Route::post('/reportes/buenoparacobro/excel', ['uses' => 'BuenoParaCobroController@exportExcelFile', 'as' => 'buenoparacobro.excel']);
//DESCARGA DE EXCEL
Route::get('/reportes/buenoparacobro/descargarExcel/{parametros?}', 'ExportExcelController@descargarExcelBuenoParaCobro');


/*
 * Tipo de Cambio
 */
Route::get('/agencia/tipocambio', ['uses' => 'TipoCambioController@index', 'as' => 'tipocambio']);
Route::get('/agencia/tipocambio/mostrar/{id}', ['uses' => 'TipoCambioController@mostrar', 'as' => 'tipocambio.mostrar']);
Route::post('/agencia/tipocambio/mostrar', ['uses' => 'TipoCambioController@mostrarRedirect', 'as' => 'tipocambio.mostrar.redirect']);
Route::post('/agencia/tipocambio/registro', ['uses' => 'TipoCambioController@actualizar', 'as' => 'tipocambio.actualizar']);
Route::get('/agencia/tipocambio/editar/{id}', ['uses' => 'TipoCambioController@editar', 'as' => 'tipocambio.editar']);
Route::post('/agencia/tipocambio/editar', ['uses' => 'TipoCambioController@actualizarAnterior', 'as' => 'tipocambio.actualizarAnterior']);

/*
 * Precio por Agencia
 */
Route::get('/agencia/precio', ['uses' => 'PrecioTourController@index', 'as' => 'precio']);
Route::get('/agencia/precio/mostrar/{agencia_id}/{paquete_tour_id}', ['uses' => 'PrecioTourController@mostrar', 'as' => 'precio.mostrar']);
Route::post('/agencia/precio/mostrar', ['uses' => 'PrecioTourController@mostrarRedirect', 'as' => 'precio.mostrar.redirect']);
Route::post('/agencia/precio/registro', ['uses' => 'PrecioTourController@actualizar', 'as' => 'precio.actualizar']);
Route::get('/agencia/precio/editar/{id}', ['uses' => 'PrecioTourController@editar', 'as' => 'precio.editar']);
Route::post('/agencia/precio/editar', ['uses' => 'PrecioTourController@actualizarAnterior', 'as' => 'precio.actualizarAnterior']);


