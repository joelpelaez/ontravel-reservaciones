<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cupones extends Model
{
    protected $table = 'cupones';
    protected $fillable = ['id_reservacion','id_tipo_cupon','n_adultos','n_ninos','n_infantes','cupon','observaciones','id_status_cupon'];

    public static function getCupones() {
    	$cupones = self::query()->orderBy('id','Asc')->get();
      	return $cupones;
    }

    public function reserva()
    {
        return $this->belongsTo('App\Reservaciones', 'id_reservacion');
    }

    public function tipoCupon()
    {
        return $this->belongsTo('App\TiposCupones','id_tipo_cupon'); 
    }
    public function statusCupon()
    {
        return $this->belongsTo('App\StatusesCupones','id_status_cupon'); 
    }
}
