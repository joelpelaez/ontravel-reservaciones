<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tours extends Model
{
    protected $table = 'tours';
    protected $fillable = ['id', 'nombre', 'descripcion', 'codigo', 'capacidad', 'id_clasificacion_tour', 'logo', 'activo'];

    public static function getTours($id_region)
    {
        $tours = self::Join('regiones_tours', 'tours.id', '=', 'regiones_tours.id_tour')
            ->where('tours.activo', 1)
            ->where('regiones_tours.id_region', '=', $id_region)
            ->orderBy('tours.nombre', 'Asc')
            ->get();

        return $tours;
    }

    public static function getToursActivos()
    {
        $tours = self::where('activo', 1)
            ->orderBy('nombre', 'Asc')
            ->get();
        return $tours;
    }

    public static function getCodigoById($id_tour)
    {
        $codigo = self::select('codigo')->where('id', '=', $id_tour)->get();
        return $codigo;
    }
    public static function getNombreById($id_tour)
    {
        $nombre = self::select('nombre')->where('id', '=', $id_tour)->get();
        return $nombre;
    }


    public function paqueteTour()
    {
        return $this->hasMany('App\PaquetesTours');
    }

    public function idiomaTour()
    {
        return $this->hasMany('App\IdiomasTours');
    }
}
