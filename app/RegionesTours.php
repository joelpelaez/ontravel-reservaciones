<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionesTours extends Model
{
    //
    public function Regiones()
    {
        return $this->hasMany('App\Regiones');
    }
    public function Tours()
    {
        return $this->hasMany('App\Tours');
    }
    public static function getRegionesByIdTour($id_tour){
        $tours = self::select('regiones.id')
            ->Join('regiones', 'regiones_tours.id_region', '=', 'regiones.id')
            ->where('regiones_tours.id_tour','=',$id_tour)
            ->orderBy('regiones.nombre','Asc')
            ->get();

        return $tours;
    }
}
