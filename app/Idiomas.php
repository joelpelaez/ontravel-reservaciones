<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Idiomas extends Model
{
    protected $table = 'idiomas';
    protected $fillable = ['id','nombre','activo'];

    public static function getIdiomasTours($id_tour){
    	$idiomastours = self::Join('idiomas_tours', 'idiomas.id', '=', 'idiomas_tours.id_idioma')
    	->where('idiomas.activo',1)
    	->where('idiomas_tours.id_tour','=',$id_tour)
    	->orderBy('idiomas.nombre','Asc')
    	->get();
    	
    	return $idiomastours;
    }

    public function idiomaTour()
    {
        return $this->hasMany('App\IdiomasTours', 'id_idioma');
    }
}
