<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCambio extends Model {
    protected $fillable = ['agencia_id', 'cambio', 'fecha_inicio', 'fecha_fin', 'activo'];

    public function agencia() {
        return $this->belongsTo(Agencias::class);
    }
}