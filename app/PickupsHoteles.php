<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickupsHoteles extends Model
{
    public static function getAreaHoteles($id_hotel)
    {
        $areahoteles = self::query()->select('area_pickup')
            ->where('activo', 1)
            ->where('id_hotel', '=', $id_hotel)
            ->orderBy('area_pickup', 'Asc')
            ->groupBy('area_pickup')
            ->get();
        return $areahoteles;
    }

    public static function getPickupsHoteles($id_hotel, $area_pickup)
    {
        $pickups = self::where('activo', 1)
            ->where('area_pickup', '=', $area_pickup)
            ->where('id_hotel', '=', $id_hotel)
            ->orderBy('pickup', 'Asc')
            ->get();
        return $pickups;
    }

    public static function getPickups($id, $id_hotel)
    {
        $pickups = self::where('activo', 1)
            ->where('id', '=', $id)
            ->where('id_hotel', '=', $id_hotel)
            ->orderBy('id', 'Asc')
            ->get();
        return $pickups;
    }

    public static function getPickupsHotelesByHotel($id_hotel)
    {
        $pickups = self::where('activo', 1)
            ->where('id_hotel', '=', $id_hotel)
            ->orderBy('id', 'Asc')
            ->get();
        return $pickups;
    }

    public function reservacion()
    {
        return $this->hasMany('App\Reservaciones');
    }
}
