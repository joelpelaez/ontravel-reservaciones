<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class PrecioTour extends Model {
    protected $fillable = ['agencia_id', 'paquete_tour_id', 'precio_adulto', 'precio_ninio', 'porcentaje_utilidad', 'fecha_inicio', 'fecha_fin', 'activo'];

    public function agencia() {
        return $this->belongsTo(Agencias::class);
    }

    public function paqueteTour() {
        return $this->belongsTo(PaquetesTours::class);
    }
}