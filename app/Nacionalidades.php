<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Nacionalidades extends Model
{
	public static function getNacionalidades(){
    	$agencias = self::where('activo',1)
    	->orderBy('nombre','Asc')
    	->get();

    	return $agencias;
    }

    public function reservacion()
    {
        return $this->hasMany('App\Reservaciones'); 
    }
    
}
