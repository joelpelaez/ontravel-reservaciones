<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposCupones extends Model
{
    protected $table = 'tipos_cupones';
    protected $fillable = ['id','nombre','activo'];

    public static function getTiposCupones(){
    	$tiposcupones = self::where('activo',1)
    	->get();
    	
    	return $tiposcupones;
    }

    public function tipoCupon()
    {
        return $this->hasMany('App\Cupones'); 
    }
}
