<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Support\Facades\DB;


class Hoteles extends Model
{
    protected $table = 'hoteles';
    protected $fillable = ['id', 'nombre', 'id_zona_hotel', 'id_zona_venta'];

    public static function getHoteles()
    {
        $hoteles = self::where('activo', 1)
            ->orderBy('nombre', 'Asc')->get();
        return $hoteles;
    }


    public function reservacion()
    {
        return $this->hasMany('App\Reservaciones');
    }


}
