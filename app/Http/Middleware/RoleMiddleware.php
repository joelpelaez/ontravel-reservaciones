<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /*
         * Get permission list from config file.
         */
        $routes = config('permission.routes');

        /*
         * Check every entry.
         */
        foreach ($routes as $key => $route) {
            if (preg_match($route['url'], $request->fullUrl()) > 0) {
                if ($request->user()) {
                    $role = $request->user()->role->name;

                    if (!in_array($role, $route['roles']) && $role !== "admin") {
                        return new Response(view('unauthorized'));
                    }
                }
            }
        }

        return $next($request);
    }

    /**
     * Check if role name is validated with roles array.
     * @param $role string The role to check.
     * @return bool true if the role is in the array, false otherwise.
     */
    public function checkRoles($role) {
        return in_array($role, $this->getAuthorizedRoles()) || $role === "admin";
    }

    /**
     * Get the array of authorized roles for the middleware.
     *
     * This method must be defined in final middleware class with authorized
     * roles for access some resources.
     * @return array The authorized roles.
     */
    function getAuthorizedRoles() {}
}
