<?php

namespace App\Http\Controllers;


use App\Agencias;
use App\TipoCambio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TipoCambioController extends Controller {
    /**
     * Muestra la página inicial de búsqueda de tipos de cambio.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View La vista 'index'
     */
    public function index() {
        $agencias = Agencias::all();
        return view('vendor/voyager/cambio/index', compact('agencias'));
    }

    public function mostrarRedirect(Request $request) {
        return redirect()->route('tipocambio.mostrar', ['id' => $request->agencia_id]);
    }

    /**
     * Muestra todos los tipos de cambio registrados en el sistema por agencia.
     * @param Request $request Información de la petición.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mostrar(Request $request, $id) {
        $agencia = Agencias::query()->find($id);
        $tiposCambio = TipoCambio::query()->where('agencia_id', '=', $id)->orderBy('fecha_inicio', 'desc')->get();

        return view('vendor/voyager/cambio/mostrar', compact('agencia', 'tiposCambio'));
    }

    /**
     * Se actualiza la información de tipo de cambio con respecto a la fecha y valores.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function actualizar(Request $request) {
        $fecha = Carbon::today();
        $anterior = $fecha->copy()->subDay();
        $cambio = $request->cambio;
        $agencia = $request->agencia_id;

        $validation = [
            'cambio' => 'required|numeric'
        ];

        $messages = [
            'cambio.numeric' => 'El Tipo de Cambio debe ser un número',
            'cambio.required' => 'El Tipo de Cambio es obligatorio'
        ];

        $this->validate($request, $validation, $messages);

        DB::beginTransaction();
        try {

            $tipoCambioDeHoy = TipoCambio::query()
                ->where('agencia_id', '=', $agencia)
                ->whereDate('fecha_inicio', '=', $fecha)
                ->get();

            /*
             * Si el tipo de cambio de hoy se cambia, afecta al mismo dia, si ya se ha realizado
             * antes la actualización del precio.
             */
            if ($tipoCambioDeHoy->count() > 0) {
                $tipoCambio = $tipoCambioDeHoy->first();
                $tipoCambio->cambio = $cambio;
                $tipoCambio->save();
            } else {
                /*
                 * Se comprueba que no existan precios en el "futuro"
                 */
                $res = TipoCambio::query()
                    ->where('agencia_id', '=', $agencia)
                    ->whereDate('fecha_inicio', '>', $fecha)
                    ->count();
                if ($res > 0) {
                    /*
                     * Se realizara un rollback y se regresara a la pantalla anterior.
                     */
                    DB::rollback();

                    return redirect()->route('tipocambio.mostrar', ['id' => $agencia]);
                }

                /*
                 * Si no aplica, entonces se registra un nuevo tipo de cambio para el periodo actual.
                 * Primero revisando cual es el actual e invalidarlo.
                 */
                $tipoCambioActual = TipoCambio::query()
                    ->where('agencia_id', '=', $agencia)
                    ->whereNull('fecha_fin')
                    ->where('activo', '=', 1)
                    ->get();

                /*
                 * Si existe un tipo de cambio activo, se marca como inactivo y la fecha de fin
                 * pasa a ser el día anterior (ayer).
                 */
                if ($tipoCambioActual->count() === 1) {
                    $tipoCambioAnterior = $tipoCambioActual->first();
                    $tipoCambioAnterior->activo = false;
                    $tipoCambioAnterior->fecha_fin = $anterior;
                    $tipoCambioAnterior->save();
                }

                /*
                 * Se crea nueva instancia del tipo de cambio para la agencia.
                 */
                $tipoCambioNuevo = new TipoCambio();
                $tipoCambioNuevo->agencia_id = $agencia;
                $tipoCambioNuevo->cambio = $cambio;
                $tipoCambioNuevo->fecha_inicio = $fecha;
                $tipoCambioNuevo->fecha_fin = null;
                $tipoCambioNuevo->activo = true;
                $tipoCambioNuevo->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            /*
             * Rethrow exception for debugging proposes.
             */
            throw $e;
        }

        DB::commit();

        return redirect()->route('tipocambio.mostrar', ['id' => $agencia]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editar(Request $request, $id) {
        $tipoCambio = TipoCambio::query()->find($id);

        $anterior = TipoCambio::query()
            ->where('agencia_id', '=', $tipoCambio->agencia_id)
            ->whereDate('fecha_fin', '=', Carbon::parse($tipoCambio->fecha_inicio)->subDay())
            ->first();

        $posterior = TipoCambio::query()
            ->where('agencia_id', '=', $tipoCambio->agencia_id)
            ->whereDate('fecha_inicio', '=', Carbon::parse($tipoCambio->fecha_fin)->addDay())
            ->first();

        $fecha_inicio_min = null;
        $fecha_fin_max = null;
        $enable_fecha_fin = $tipoCambio->fecha_fin !== null;

        if ($anterior !== null) {
            $fecha_inicio_min = Carbon::parse($anterior->fecha_inicio)->addDay()->toDateString();
        }

        if ($posterior !== null) {
            $fecha_fin_max = Carbon::parse($posterior->fecha_fin)->subDay()->toDateString();
        }

        $cambio = $tipoCambio->cambio;
        $fecha_inicio = $tipoCambio->fecha_inicio;
        $fecha_fin = $tipoCambio->fecha_fin;

        return view('vendor/voyager/cambio/editar',
            compact('id', 'cambio', 'fecha_inicio', 'fecha_fin',
                'enable_fecha_fin', 'fecha_fin_max', 'fecha_inicio_min'));
    }

    public function actualizarAnterior(Request $request) {
        $id = $request->id;
        $cambio = $request->cambio;
        $fecha_inicio = $request->fecha_inicio;
        $fecha_fin = $request->fecha_fin;

        /*
         * Se evaluará que valores han cambiado para proceder a actualizarlos.
         */
        $cambioActual = TipoCambio::query()->find($id);
        $cambioAnterior = null;
        $cambioSiguiente = null;

        $validation = [
            'cambio' => 'required|numeric'
        ];

        $messages = [
            'cambio.numeric' => 'El Tipo de Cambio debe ser un número',
            'cambio.required' => 'El Tipo de Cambio es obligatorio'
        ];

        if ($fecha_inicio !== $cambioActual->fecha_inicio) {
            /*
             * Se buscara si existe algun registro anterior a este para actualizarlo en conjunto.
             */
            $fechaAnterior = Carbon::parse($cambioActual->fecha_inicio)->subDay();
            $cambioAnterior = TipoCambio::query()
                ->where('agencia_id', '=', $cambioActual->agencia_id)
                ->whereDate('fecha_fin', '=', $fechaAnterior)
                ->first();

            if ($cambioAnterior !== null) {
                $fecha = date_create($cambioAnterior->fecha_inicio);
                if ($cambioActual->fecha_fin !== null)
                    $validation['fecha_inicio'] = "required|date|before_or_equal:fecha_fin|after_or_equal:{$cambioAnterior->fecha_inicio}";
                else
                    $validation['fecha_inicio'] = "required|date|before_or_equal:today|after_or_equal:{$cambioAnterior->fecha_inicio}";
                $messages['fecha_inicio.after_or_equal'] = "La Fecha Inicio del periodo debe ser mayor a {$fecha->format('d/m/Y')}";
            } else if ($cambioActual->fecha_fin !== null) {
                $validation['fecha_inicio'] = "required|date|before_or_equal:fecha_fin";
            } else {
                $validation['fecha_inicio'] = "required|date|before_or_equal:today";
            }

            $messages['fecha_inicio.required'] = "La Fecha Inicio es obligatoria";
            $messages['fecha_inicio.before_or_equal'] = "La Fecha Inicio debe ser menor o igual a Fecha Fin";
        }

        if ($cambioActual->fecha_fin !== null && $fecha_fin !== $cambioActual->fecha_fin) {
            /*
             * Se buscara si existe algun registro posterior a este para actualizarlo en conjunto.
             */
            $fechaPosterior = Carbon::parse($cambioActual->fecha_fin)->addDay();
            $cambioSiguiente = TipoCambio::query()
                ->where('agencia_id', '=', $cambioActual->agencia_id)
                ->whereDate('fecha_inicio', '=', $fechaPosterior)
                ->first();

            if ($cambioSiguiente !== null) {
                if ($cambioSiguiente->fecha_fin !== null) {
                    $fecha = date_create($cambioSiguiente->fecha_fin);
                    $validation['fecha_fin'] = "required|date|after_or_equal:fecha_inicio|before_or_equal:{$cambioSiguiente->fecha_fin}";
                    $messages['fecha_fin.before_or_equal'] = "La Fecha Fin del periodo debe ser menor o igual a {$fecha->format('d/m/Y')}";
                } else {
                    $ayer = Carbon::today()->subDay();
                    $validation['fecha_fin'] = "required|date|after_or_equal:fecha_inicio|before_or_equal:{$ayer->toDateString()}";
                    $messages['fecha_fin.before_or_equal'] = "La Fecha Fin del periodo debe ser menor o igual a {$ayer->format('d/m/Y')}";
                }
            } else {
                $validation['fecha_fin'] = "required|date|after_or_equal:fecha_inicio";
            }
            $messages['fecha_fin.required'] = "La Fecha Fin es obligatoria";
            $messages['fecha_fin.after_or_equal'] = "La Fecha Fin debe ser mayor o igual a la Fecha Inicio";
        }

        $this->validate($request, $validation, $messages);

        /*   z<
         * Se inicia el proceso de actualización.
         */
        DB::beginTransaction();

        try {
            if ($cambioAnterior !== null) {
                $fechaAnteriorNueva = Carbon::parse($fecha_inicio)->subDay();
                $cambioAnterior->fecha_fin = $fechaAnteriorNueva;
                $cambioAnterior->save();
            }

            if ($cambioSiguiente !== null) {
                $fechaPosteriorNueva = Carbon::parse($fecha_fin)->addDay();
                $cambioSiguiente->fecha_inicio = $fechaPosteriorNueva;
                $cambioSiguiente->save();
            }

            $cambioActual->fecha_inicio = $fecha_inicio;
            $cambioActual->fecha_fin = $fecha_fin;
            $cambioActual->cambio = $cambio;
            $cambioActual->save();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();


        return redirect()->route('tipocambio.mostrar', ['id' => $cambioActual->agencia_id]);
    }
}