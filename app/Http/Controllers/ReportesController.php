<?php

namespace App\Http\Controllers;

use App\Agencias;
use App\Reservaciones;
use App\Tours;
use Illuminate\Http\Request;

class ReportesController extends Controller
{
    public function reportes()
    {
        return view('/vendor/voyager/reportes/reportes');
    }

    public function buscarOrdenDeServicio()
    {
        $tours = Tours::getToursActivos();

        return view('/vendor/voyager/reportes/buscar/orden_de_servicio', compact('tours'));
    }

    public function resultadosOrdenDeServicio(Request $request)
    {
        $validation = [
            'fecha_reservacion' => 'required|date',
            'id_tour' => 'required'
        ];

        $messages = [
            'id_tour.numeric' => 'El Tour no existe',
            'id_tour.required' => 'Seleccione un Tour',
            'fecha_reservacion.required' => 'Debe ingresar la Fecha de Reservación',
            'fecha_reservacion.date' => 'La Fecha de Reservación tiene un formato incorrecto',
        ];

        $this->validate($request, $validation, $messages);

        $reservas = array();
        if ($request->fecha_reservacion != null) {
            $reservas = Reservaciones::getReservacionesByFechaByTour($request->fecha_reservacion, $request->id_tour);
        }
        $parametros = $request->all();
        $id_tour = $request->id_tour;
        $fecha_reservacion = $request->fecha_reservacion;
        $parametros = json_encode($parametros);
        return view('/vendor/voyager/reportes/resultados/orden_de_servicio', compact('reservas', 'parametros', 'id_tour', 'fecha_reservacion'));
    }

    public function buscarBuenoParaCobro()
    {
        $tours = Tours::getToursActivos();
        $agencias = Agencias::getAgencias();

        return view('/vendor/voyager/reportes/buscar/bueno_para_cobro', compact('tours', 'agencias'));
    }

    public function resultadosBuenoParaCobro(Request $request)
    {
        $validation = [
            'fecha_inicio' => 'nullable|date',
            'fecha_fin' => 'nullable|date',
            'id_agencia' => 'nullable|numeric',
            'id_tour' => 'nullable|numeric'
        ];

        $messages = [
            'id_agencia.numeric' => 'La Agencia es incorrecta',
            'id_agencia.required' => 'Seleccione una Agencia',
            'id_tour.numeric' => 'El Tour no existe',
            'id_tour.required' => 'Seleccione un Tour',
            'fecha_inicio.required' => 'Debe ingresar la Fecha Inicio',
            'fecha_inicio.date' => 'La Fecha Inicio tiene un formato incorrecto',
            'fecha_fin.required' => 'Debe ingresar la Fecha Fin',
            'fecha_fin.date' => 'La Fecha Fin tiene un formato incorrecto'
        ];

        $this->validate($request, $validation, $messages);

        $reservas = Reservaciones::obtenerReservacionesPorIdTourYPorRangoDeFechasYIdAgencia($request->fecha_inicio, $request->fecha_fin, $request->id_tour, $request->id_agencia);

        $parametros = $request->all();
        $parametros = json_encode($parametros);
        $fecha_inicio = $request->fecha_inicio;
        $fecha_fin = $request->fecha_fin;
        $agencia_id = $request->id_agencia;
        $tour_id = $request->id_tour;
        return view('/vendor/voyager/reportes/resultados/bueno_para_cobro',
            compact('reservas', 'parametros', 'fecha_inicio', 'fecha_fin', 'agencia_id', 'tour_id'));
    }
}
