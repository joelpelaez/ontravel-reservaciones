<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservaciones;
use App\Cupones;
use App\Tours;
use App\PaquetesTours;

use Jenssegers\Date\Date;

class ExportExcelController extends Controller
{
    
	public function descargarExcelBuenoParaCobro($parametros){
    
      $request = json_decode($parametros);

      $reservaciones = array();
        if ($request->fecha_inicio != null AND $request->fecha_fin != null AND $request->id_tour != null) {

            $reservaciones = Reservaciones::obtenerReservacionesPorIdTourYPorRangoDeFechasYIdAgencia($request->fecha_inicio, $request->fecha_fin,$request->id_tour,$request->id_agencia);
        } 
        
        $tour = Tours::getNombreById($request->id_tour); 
        $fecha_inicio = new Date($request->fecha_inicio);
        $fecha_fin = new Date($request->fecha_fin);
        $paquetes = PaquetesTours::paquetes($request->id_tour);
        

$table = '<div class="container-fluid">
<!-- ==================== TABLE ROW ==================== -->
            <div class="row-fluid">
              <div class="span12">
            <div class="containerHeadline" style="background-color:#a9d08e;">
              <table>
                <tr>
                  <td colspan="7">
                    <h4>'.$tour[0]['nombre'].' '.$fecha_inicio->format('j').' AL '.$fecha_fin->format('j \d\e F \d\e\l Y').'</h4>
                  </td>
                </tr>
              </table>         
            </div>';
  
  $table.='
      <div class="floatingBox table">
        <div class="container-fluid">
        <table class="table" border="1">
          <thead>
            <tr>
            <th colspan="7"></th>
            <th style="background-color:#ffe699;" colspan="2">Adultos</th>
            <th style="background-color:#f8cbad;" colspan="2">Menores</th>
            <th style="background-color:#ffe699;" colspan="2">Adultos</th>
            <th style="background-color:#f8cbad;" colspan="2">Menores</th>
            </tr>
            <tr>
              <th style="background-color:#a9d08e;">AGENCIA</th>
              <th style="background-color:#a9d08e;">TOUR</th>
              <th style="background-color:#a9d08e;">FECHA</th>
              <th style="background-color:#a9d08e;">CUPON VIDANTA</th>
              <th style="background-color:#a9d08e;">NOMBRE</th>
              <th style="background-color:#a9d08e;">N PAX</th>
              <th style="background-color:#a9d08e;">PAQUETE</th>';
              for ($i=0; $i <=3 ; $i++) { 
                foreach ($paquetes as $paquete) {
                   if($i == 0){
                    $table.='<th style="background-color:#ffe699;">'.utf8_decode($paquete->nombre).'</th>';
                    }
                    if($i == 1){
                    $table.='<th style="background-color:#f8cbad;">'.utf8_decode($paquete->nombre).'</th>';
                    }
                    if($i == 2){
                     $table.='<th style="background-color:#ffe699;">COSTO '.utf8_decode($paquete->nombre).' CON IVA</th>';
                    }
                    if($i == 3){
                     $table.='<th style="background-color:#f8cbad;">COSTO '.utf8_decode($paquete->nombre).' CON IVA</th>';
                    }
                }
              }
             
    $table.='
            </tr>
          </thead>
          <tbody>';

            $total_pax_cupones=0;
            foreach ($reservaciones as $key => $reserva) {

              $cupones = Cupones::where('id_reservacion', '=', $reserva->id)->get();
                    
                $cupon_tabla = "";
                $total_pax = 0; 

              foreach ($cupones as $cupon) {
                $total_pax = $cupon->n_adultos + $cupon->n_ninos;
                $cupon_tabla = $cupon->cupon;
                $total_pax_cupones +=  $total_pax;
              } 

              $table.='<tr>';
              $table.='<td>'.$reserva->agencia->nombre.'</td>';
              $table.='<td>'.$reserva->paqueteTour->tour->nombre.'</td>';
              $table.='<td>'.$reserva->fecha_reservacion->format('d-F-Y').'</td>';
              $table.='<td>'.$cupon_tabla.'</td>';
              $table.='<td>'.$reserva->apellido.'</td>';
              $table.='<td>'.$total_pax.'</td>';
              $table.='<td>'.utf8_decode($reserva->paqueteTour->paquete->nombre).'</td>';   
              $table.='</tr>';
            
          }

          $table.='
          </tbody>
        </table>';
         $table.='
                <table>
                      <tr>
                        <td colspan="5"></td>
                        <td border="1"><b>'.$total_pax_cupones.'</b></td>
                      </tr>
                </table>';
  $table.='</div>
              </div>
                </div>
                  </div>
                    </div>';

    header("Content-Type: application/vnd.ms-excel; charset=utf-8");
    header("Content-type: application/x-msexcel; charset=utf-8");
    header("Content-Disposition: attachment; filename=bueno_para_cobro.xls");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");    
    $html = '<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"><title>Bueno para cobro</title><style type="text/css">
        td, th{white-space:nowrap; padding:0px; margin:0px; text-align:center; font-size:12px;}
        table{width:100%;}</style></head><body>';
    $html.=$table;
    $html.='</body></html>';
    return $html;
	}


  public function descargarExcelOrdenServicio($parametros){
    
      $request = json_decode($parametros);
      $reservaciones = array();
        if ($request->fecha_reservacion != null && $request->id_tour != null) {
             $reservaciones = Reservaciones::getReservacionesByFechaByTour($request->fecha_reservacion,$request->id_tour);
        }

        $tour = Tours::getNombreById($request->id_tour); 
        $fecha = new Date($request->fecha_reservacion);


        $table = '<div class="container-fluid">

                <div class="row-fluid">
                    <div class="span12">
                    <div class="containerHeadline">
                      <table class="table">
                        <thead>
                            
                        </thead>
                        <tbody>
                          <tr>
                          <td>Horario del operador:</td>
                          <td colspan="2">&nbsp;</td>
                          <td>Horario del Guia:</td>
                          <td colspan="2">&nbsp;</td>
                          </tr>
                          <tr>
                          <td>Operador(es):</td>
                          <td colspan="2">&nbsp;</td>
                          <td>Guia(s):</td>
                          <td colspan="2">&nbsp;</td>
                          </tr>
                          <tr>
                          <td>Trainning staff:</td>
                           <td colspan="2">&nbsp;</td>
                          <td>Fotografos:</td>
                           <td colspan="2">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    </div>
  
        <div class="containerHeadline" style="background-color:#a9d08e;">
        <table>
          <tr>
          <td></td>
          <td></td>
          <td colspan="7" style="background-color:#a9d08e;"><h1>'.$tour[0]['nombre'].'</h4>
          </td>
          <td><b>Folio del servicio:</b></td>
          <td colspan="2"></td>
          </tr>
          <tr  style="background-color:#a9d08e;">
          <td colspan="12">
            <h3 style="text-align: center; background-color:#a9d08e;">'.$fecha->format('l j \d\e F \d\e\l Y').' </h3>
          </td>  
          
          </tr>
        </table>         
        </div>
  
      <div class="floatingBox table">
        <div class="container-fluid">
          <table class="table" border="1">
            <thead>
              <tr>
                <th>Folio Cupon</th>
                <th>Nombre de Reserva</th>
                <th>Habitacion</th>
                <th>Idioma</th>
                <th>Hotel</th>
                <th>No. Pax</th>
                <th>Vendedor</th>
                <th>Agencia</th>
                <th>Confirmacion</th>
                <th>Paquete</th>
                <th>Observaciones</th>
                <th>Chofer</th>
              </tr>
            </thead>
            <tbody>';
            $total_pax_cupones=0;
            foreach ($reservaciones as $key => $reserva) {

                $cupones = Cupones::where('id_reservacion', '=', $reserva->id)->get();
                
                $cupon_tabla = "";
                $total_pax = 0;
                $observaciones = "";
            
                foreach ($cupones as $cupon) {

                  $cupon_tabla = $cupon->cupon;
                  $total_pax = $cupon->n_adultos + $cupon->n_ninos;
                  $total_pax_cupones += $total_pax;
                  $observaciones = $cupon->observaciones;  

                }  

                $table.='<tr  border="1">';
                $table.='<td>'.$cupon_tabla.'</td>';
                $table.='<td>'.utf8_decode($reserva->apellido).'</td>';
                $table.='<td>'.$reserva->habitacion.'</td>';
                $table.='<td>'.utf8_decode($reserva->idiomaTour->idioma->nombre).'</td>';
                $table.='<td>'.utf8_decode($reserva->hotel->nombre).'</td>';
                $table.='<td>'.$total_pax.'</td>';
                $table.='<td>'.utf8_decode($reserva->representante->nombre).'</td>';
                $table.='<td>'.utf8_decode($reserva->agencia->nombre).'</td>';
                $table.='<td>'.$reserva->confirmacion.'</td>';
                $table.='<td>'.utf8_decode($reserva->paqueteTour->paquete->nombre).'</td>';
                $table.='<td>'.utf8_decode($observaciones).'</td>';
                $table.='<td></td>';
                $table.='</tr>';
            }
           
            $table.='
            </tbody>
            </table>';
            $table.='<table>
                      <tr>
                        <td colspan="5"></td>
                        <td border="1"><b>'.$total_pax_cupones.'</b></td>
                        <td colspan="6"></td>
                      </tr>
                      <tr>
                        <td><h4>NOTAS:</h4></td>
                        <td colspan="11"></td>
                      </tr>
                    </table>';
        $table.='
        </div>
        </div>
        </div>
        </div>
        </div>';
    
      header("Content-Type: application/vnd.ms-excel; charset=utf-8");
      header ("Content-type: application/x-msexcel; charset=utf-8");
      header("Content-Disposition: attachment; filename=orden_de_servicio.xls");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");    

      $html = '<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"><title>Orden de servicio</title><style type="text/css">
          td, th{white-space:nowrap; padding:0px; margin:0px; text-align:center; font-size:12px;}
          table{width:100%;}
          </style>
          </head><body>';

      $html.=$table;
      $html.='</body></html>';

    return $html;
  }

  
  




}
