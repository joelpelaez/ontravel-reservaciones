<?php

namespace App\Http\Controllers;

use App\Reservaciones;
use App\Tours;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as XlsxReader;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class OrdenServicioExporterController extends Controller
{
    public function exportExcelFileFromTemplate(Request $request)
    {
        /*
         * Se obtienen los parametros de busqueda
         */
        $fecha_reservacion = $request->fecha_reservacion;
        $id_tour = $request->id_tour;

        $reservaciones = Reservaciones::getReservacionesByFechaByTour($fecha_reservacion, $id_tour);
        $tour = Tours::query()->find($id_tour);

        if ($tour === null || $reservaciones->count() <= 0)
            return view('vendor/voyager/reportes/error');

        $processed = $this->prepareData($reservaciones);

        /*
         * Crear una nueva hoja de calculo
         */
        $loader = new XlsxReader();
        $spreadsheet = $loader->load(base_path() . '/resources/files/formato_ordenes.xlsx');

        /*
         * Fila base
         */
        $base_row = 13;

        /*
         * Nombre del archivo a generar
         */
        $filename = "OrdenDeServicio_{$tour->nombre}_$fecha_reservacion.xlsx";

        try {
            $worksheet = $spreadsheet->getActiveSheet();

            /*
             * Información general de la hoja de cálculo, esta en el rango de B2:G5
             */
            $excelDate = Date::PHPToExcel($fecha_reservacion);
            $worksheet->getCell('K8')->setValue($tour->nombre);
            $worksheet->getCell('J12')->setValue($excelDate);

            /*
             * Se iteran cada una de las reservas.
             */
            $adultos = $ninos = 0;
            if (count($processed) > 1)
                $worksheet->insertNewRowBefore(15, count($processed) - 1);
            foreach ($processed as $key => $reserva) {
                $adultos += $reserva->n_adultos;
                $ninos += $reserva->n_ninos;

                $row = $key + $base_row + 1;

                $excelDate = Date::PHPToExcel($reserva->fecha);
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($excelDate)
                    ->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($reserva->apellido);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($reserva->idioma);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($reserva->pax);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($reserva->paquete);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($reserva->cupon);
                $worksheet->getCellByColumnAndRow(7, $row)->setValue($reserva->origen);
                $worksheet->getCellByColumnAndRow(8, $row)->setValue($reserva->habitacion);
                $time = new \DateTime($reserva->pickup, new \DateTimeZone('UTC'));
                $excelHour = Date::PHPToExcel($time->format('U'));
                $worksheet->getCellByColumnAndRow(9, $row)->setValue($excelHour)
                    ->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_DATE_TIME3);
                $worksheet->getCellByColumnAndRow(10, $row)->setValue($reserva->agencia);
                $worksheet->getCellByColumnAndRow(11, $row)->setValue($reserva->representante);
                $worksheet->getCellByColumnAndRow(12, $row)->setValue($reserva->confirmacion);
            }

            /*
             * Se calcula el rango de datos.
             */
            $base = $worksheet->getCellByColumnAndRow(1, $base_row)->getCoordinate();
            $max = $worksheet->getCellByColumnAndRow(13, count($processed) + $base_row)->getCoordinate();

            $rango = $base . ':' .$max;

            /*
             * Rango 'Reservas' con los datos de la tabla.
             */
            $spreadsheet->addNamedRange(new NamedRange('Reservas', $worksheet, $rango));

            $worksheet->getAutoFilter()->setRange($rango);

            if ($ninos > 0) {
                $worksheet->getCellByColumnAndRow(4, count($processed) + $base_row + 1)->setValue($adultos . '.' . $ninos);
            } else {
                $worksheet->getCellByColumnAndRow(4, count($processed) + $base_row + 1)->setValue($adultos);
            }

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');

            $writer = new XlsxWriter($spreadsheet);
            $writer->save('php://output');
        } catch (Exception $e) {
            return view('vendor/voyager/reportes/error');
        }
        die();
    }

    /**
     * Process data from source and create objects with filtered attributes.
     *
     * Get a dataset from the source and extract and process fields for show them.
     *
     * @param $reservaciones
     * @return array
     */
    protected function prepareData($reservaciones)
    {
        $clearData = [];
        foreach ($reservaciones as $reservacion) {
            foreach ($reservacion->cupon as $cupon) {
                $obj = new \stdClass();
                $obj->fecha = $reservacion->fecha_reservacion->format('d/m/Y');
                $obj->apellido = $reservacion->apellido;

                $obj->idioma = $reservacion->idiomaTour->idioma->nombre;

                $obj->n_adultos = $cupon->n_adultos;

                $obj->n_ninos = $cupon->n_ninos;

                if ($cupon->n_ninos > 0) {
                    $obj->pax = $cupon->n_adultos . '.' . $cupon->n_ninos;
                } else {
                    $obj->pax = $cupon->n_adultos;
                }

                $obj->cupon = $cupon->cupon;

                $obj->paquete = $reservacion->paqueteTour->paquete->nombre;

                if ($reservacion->hotel !== null) {
                    $obj->origen = $reservacion->hotel->nombre;
                } else
                    $obj->origen = 'En Dirección';

                if ($reservacion->habitacion !== null) {
                    $obj->habitacion = $reservacion->habitacion;
                } else
                    $obj->habitacion = 'N/A';
                if ($reservacion->pickup !== null) {
                    $obj->pickup = $reservacion->pickup->pickup;
                } else {
                    $obj->pickup = $reservacion->pickup_hora;
                }
                if ($reservacion->agencia !== null) {
                    $obj->agencia = $reservacion->agencia->nombre;
                } else
                    $obj->agencia = 'Sin Agencia';
                if ($reservacion->representante !== null) {
                    $obj->representante = $reservacion->representante->nombre;
                } else
                    $obj->representante = 'Personal';

                $obj->observaciones = $reservacion->observaciones;

                $obj->confirmacion = $cupon->confirmacion;

                $clearData[] = $obj;
            }
        }

        return $clearData;
    }

}