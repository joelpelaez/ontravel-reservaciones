<?php

namespace App\Http\Controllers;

use App\Reservaciones;
use App\Tours;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Shared\TimeZone;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class BusquedaExporterController extends Controller
{

    /**
     * Create a Excel file for Search
     *
     * Create a Excel spreadsheet from search paramenters for extract the results
     * for data treatment.
     *
     * @param Request $request Search request.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View Return a view if a exception exists.
     */
    public function exportExcelFile(Request $request)
    {
        /*
         * Los parametros se pasan directo a la búsqueda
         */
        $reservaciones = Reservaciones::obtenerReservaciones($request);

        if ($reservaciones->count() <= 0)
            return view('vendor/voyager/reportes/error');

        $processed = $this->prepareData($reservaciones);

        /*
         * Crear una nueva hoja de calculo
         */
        $spreadsheet = new Spreadsheet();

        if (!TimeZone::setTimeZone('America/Cancun')) {
            return view('vendor/voyager/reportes/error');
        }

        /*
         * Establecer los metadatos
         */
        $spreadsheet->getProperties()
            ->setCreator("OnTravel Reservaciones")
            ->setLastModifiedBy("Sistema de Reservas")
            ->setTitle("Búsqueda de reservaciones")
            ->setSubject("Búsqueda de reservaciones - Resultados")
            ->setDescription("Hoja de cálculo de las reservaciones por búsqueda")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Búsqueda de Reservaciones");

        /*
         * Fila base
         */
        $base_row = 7;

        /*
         * Nombre del archivo a generar
         */
        $now = now();
        $filename = "BusquedaReservas_{$now->format('Y-m-d_H-i')}.xlsx";

        try {
            $worksheet = $spreadsheet->getActiveSheet();
            $worksheet->setTitle('Resultados');

            /*
             * Se definen todos los tamaños de las columnas.
             */
            $worksheet->getDefaultColumnDimension()->setAutoSize(true);
            $worksheet->getColumnDimension('B')->setWidth(14);
            $worksheet->getColumnDimension('C')->setWidth(14);
            $worksheet->getColumnDimension('D')->setWidth(14);
            $worksheet->getColumnDimension('E')->setWidth(14);
            $worksheet->getColumnDimension('F')->setWidth(14);
            $worksheet->getColumnDimension('G')->setWidth(12);
            $worksheet->getColumnDimension('H')->setWidth(10);
            $worksheet->getColumnDimension('I')->setWidth(12);
            $worksheet->getColumnDimension('J')->setWidth(12);
            $worksheet->getColumnDimension('K')->setWidth(12);
            $worksheet->getColumnDimension('L')->setWidth(12);
            $worksheet->getColumnDimension('M')->setWidth(15);
            $worksheet->getColumnDimension('N')->setWidth(15);
            $worksheet->getColumnDimension('O')->setWidth(15);
            $worksheet->getColumnDimension('P')->setWidth(25);
            $worksheet->getColumnDimension('Q')->setWidth(25);

            /*
             * Información general de la hoja de cálculo, esta en el rango de B2:G5
             */
            $worksheet->getCell('B2')->setValue('Búsqueda de reservas');
            $worksheet->getCell('B3')->setValue('Fecha:');
            $excelDate = Date::PHPToExcel($now);
            $worksheet->getCell('C3')->setValue($excelDate)
                ->getStyle()->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_DATETIME);

            /*
             * Nombre de las columnas.
             */
            $worksheet->getCellByColumnAndRow(2, $base_row)->setValue('Fecha Venta'); //A
            $worksheet->getCellByColumnAndRow(3, $base_row)->setValue('Fecha Reservación'); //B
            $worksheet->getCellByColumnAndRow(4, $base_row)->setValue('Apellido'); //C
            $worksheet->getCellByColumnAndRow(5, $base_row)->setValue('Tour'); //D
            $worksheet->getCellByColumnAndRow(6, $base_row)->setValue('Paquete'); //E
            $worksheet->getCellByColumnAndRow(7, $base_row)->setValue('Idioma'); //F
            $worksheet->getCellByColumnAndRow(8, $base_row)->setValue('PAX'); //G
            $worksheet->getCellByColumnAndRow(9, $base_row)->setValue('Cupón'); //H
            $worksheet->getCellByColumnAndRow(10, $base_row)->setValue('Confirmación'); //I
            $worksheet->getCellByColumnAndRow(11, $base_row)->setValue('Origen'); //J
            $worksheet->getCellByColumnAndRow(12, $base_row)->setValue('Habitación'); //K
            $worksheet->getCellByColumnAndRow(13, $base_row)->setValue('Lugar de Pickup'); //L
            $worksheet->getCellByColumnAndRow(14, $base_row)->setValue('Pickup'); //M
            $worksheet->getCellByColumnAndRow(15, $base_row)->setValue('Agencia'); //N
            $worksheet->getCellByColumnAndRow(16, $base_row)->setValue('Representante'); //O
            $worksheet->getCellByColumnAndRow(17, $base_row)->setValue('Observaciones'); //P

            /*
             * Se iteran cada una de las reservas.
             */
            foreach ($processed as $key => $reserva) {
                $row = $key + $base_row + 1;
                $excelDate = Date::PHPToExcel($reserva->fecha_venta);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($excelDate)
                    ->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                $excelDate = Date::PHPToExcel($reserva->fecha);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($excelDate)
                    ->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($reserva->apellido);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($reserva->tour);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($reserva->paquete);
                $worksheet->getCellByColumnAndRow(7, $row)->setValue($reserva->idioma);
                $worksheet->getCellByColumnAndRow(8, $row)->setValue($reserva->pax);
                $worksheet->getCellByColumnAndRow(9, $row)->setValue($reserva->cupon);
                $worksheet->getCellByColumnAndRow(10, $row)->setValue($reserva->confirmacion);
                $worksheet->getCellByColumnAndRow(11, $row)->setValue($reserva->origen);
                $worksheet->getCellByColumnAndRow(12, $row)->setValue($reserva->habitacion);
                $worksheet->getCellByColumnAndRow(13, $row)->setValue($reserva->lugar_pickup);
                $worksheet->getCellByColumnAndRow(14, $row)->setValue($reserva->pickup);
                $worksheet->getCellByColumnAndRow(15, $row)->setValue($reserva->agencia);
                $worksheet->getCellByColumnAndRow(16, $row)->setValue($reserva->representante);
                $worksheet->getCellByColumnAndRow(17, $row)->setValue($reserva->observaciones);
            }

            /*
             * Se calcula el rango de datos.
             */
            $base = $worksheet->getCellByColumnAndRow(2, $base_row)->getCoordinate();
            $max = $worksheet->getCellByColumnAndRow(17, count($processed) + $base_row)->getCoordinate();

            $rango = $base . ':' .$max;

            /*
             * Rango 'Reservas' con los datos de la tabla.
             */
            $spreadsheet->addNamedRange(new NamedRange('Reservas', $worksheet, $rango));

            $worksheet->getAutoFilter()->setRange($rango);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');

            $writer = new XlsxWriter($spreadsheet);
            $writer->save('php://output');
        } catch (Exception $e) {
            return view('vendor/voyager/reportes/error');
        }
        die();
    }

    protected function prepareData($reservaciones)
    {
        $clearData = [];
        foreach ($reservaciones as $reservacion) {
            foreach ($reservacion->cupon as $cupon) {
                $obj = new \stdClass();
                $obj->fecha_venta = $reservacion->fecha_venta->format('d/m/Y');
                $obj->fecha = $reservacion->fecha_reservacion->format('d/m/Y');
                $obj->apellido = $reservacion->apellido;
                $obj->tour = $reservacion->paqueteTour->tour->nombre;
                $obj->paquete = $reservacion->paqueteTour->paquete->nombre;

                $obj->idioma = $reservacion->idiomaTour->idioma->nombre;

                if ($cupon->n_ninos > 0) {
                    $obj->pax = $cupon->n_adultos . '.' . $cupon->n_ninos;
                } else {
                    $obj->pax = $cupon->n_adultos;
                }

                $obj->cupon = $cupon->cupon;
                $obj->confirmacion = $cupon->confirmacion;

                if ($reservacion->hotel !== null) {
                    $obj->origen = $reservacion->hotel->nombre;
                } else
                    $obj->origen = 'En Dirección';

                if ($reservacion->habitacion !== null) {
                    $obj->habitacion = $reservacion->habitacion;
                } else {
                    $obj->habitacion = 'N/A';
                }

                if ($reservacion->pickup !== null) {
                    $obj->lugar_pickup = $reservacion->pickup->area_pickup;
                    $obj->pickup = $reservacion->pickup->pickup;
                } else {
                    $obj->lugar_pickup = $reservacion->direccion;
                    $obj->pickup = date('H:i', strtotime($reservacion->pickup_hora));
                }

                if ($reservacion->agencia !== null) {
                    $obj->agencia = $reservacion->agencia->nombre;
                } else {
                    $obj->agencia = 'Sin Agencia';
                }

                if ($reservacion->representante !== null) {
                    $obj->representante = $reservacion->representante->nombre;
                } else {
                    $obj->representante = 'Personal';
                }

                $obj->observaciones = $cupon->observaciones;

                $clearData[] = $obj;
            }
        }

        return $clearData;
    }

}