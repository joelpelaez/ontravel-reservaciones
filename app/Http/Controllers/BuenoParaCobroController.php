<?php

namespace App\Http\Controllers;

use App\Reservaciones;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class BuenoParaCobroController extends Controller
{
    /**
     * Create a Excel file for Bueno Para Cobro
     *
     * Create a Excel spreadsheet in this controller action using 'reservaciones' data and
     * build a 'Bueno para Cobro' relation.
     *
     * @param Request $request information about search
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception rethrows base exception for debuging.
     */
    public function exportExcelFile(Request $request) {
        $fecha_inicio = $request->fecha_inicio;
        $fecha_fin = $request->fecha_fin;
        $tour_id = $request->tour_id;
        $agencia_id = $request->agencia_id;

        $reservaciones = Reservaciones::obtenerBuenoParaCobro($fecha_inicio, $fecha_fin, $agencia_id, $tour_id);

        if ($reservaciones->count() <= 0)
            return view('vendor/voyager/reportes/error');

        /*
         * Crear una nueva hoja de calculo
         */
        $spreadsheet = new Spreadsheet();

        /*
         * Fila base
         */
        $base_row = 1;

        /*
         * Nombre del archivo a generar
         */
        $hoy = Carbon::today();
        $filename = "BuenoParaCobro_{$hoy->toDateString()}.xlsx";

        try {
            $worksheet = $spreadsheet->getActiveSheet();
            $worksheet->setTitle('Reservaciones');

            /*
             * Se definen todos los tamaños de las columnas.
             */
            $worksheet->getDefaultColumnDimension()->setAutoSize(true);
            $worksheet->getColumnDimension('A')->setWidth(20);
            $worksheet->getColumnDimension('B')->setWidth(20);
            $worksheet->getColumnDimension('C')->setWidth(18);
            $worksheet->getColumnDimension('D')->setWidth(10);
            $worksheet->getColumnDimension('E')->setWidth(16);
            $worksheet->getColumnDimension('F')->setWidth(12);
            $worksheet->getColumnDimension('G')->setWidth(12);
            $worksheet->getColumnDimension('H')->setWidth(20);
            $worksheet->getColumnDimension('I')->setWidth(12);
            $worksheet->getColumnDimension('J')->setWidth(12);
            $worksheet->getColumnDimension('K')->setWidth(18);
            $worksheet->getColumnDimension('L')->setWidth(18);
            $worksheet->getColumnDimension('M')->setWidth(20);
            $worksheet->getColumnDimension('N')->setWidth(20);
            $worksheet->getColumnDimension('O')->setWidth(20);
            $worksheet->getColumnDimension('P')->setWidth(20);
            $worksheet->getColumnDimension('Q')->setWidth(20);
            $worksheet->getColumnDimension('R')->setWidth(20);
            $worksheet->getColumnDimension('S')->setWidth(20);
            $worksheet->getColumnDimension('T')->setWidth(20);


            /*
             * Nombre de las columnas.
             */
            $worksheet->getCellByColumnAndRow(1, $base_row)->setValue('Empresa');
            $worksheet->getCellByColumnAndRow(2, $base_row)->setValue('Fecha Venta');
            $worksheet->getCellByColumnAndRow(3, $base_row)->setValue('Fecha Reserva');
            $worksheet->getCellByColumnAndRow(4, $base_row)->setValue('Estado');
            $worksheet->getCellByColumnAndRow(5, $base_row)->setValue('Confirmación');
            $worksheet->getCellByColumnAndRow(6, $base_row)->setValue('Cliente');
            $worksheet->getCellByColumnAndRow(7, $base_row)->setValue('Vendedor');
            $worksheet->getCellByColumnAndRow(8, $base_row)->setValue('Paquete');
            $worksheet->getCellByColumnAndRow(9, $base_row)->setValue('Adultos');
            $worksheet->getCellByColumnAndRow(10, $base_row)->setValue('Niños');
            $worksheet->getCellByColumnAndRow(11, $base_row)->setValue('Tipo Cambio');
            $worksheet->getCellByColumnAndRow(12, $base_row)->setValue('Costo Adulto USD');
            $worksheet->getCellByColumnAndRow(13, $base_row)->setValue('Costo Niño USD');
            $worksheet->getCellByColumnAndRow(14, $base_row)->setValue('Costo Adulto USD Total');
            $worksheet->getCellByColumnAndRow(15, $base_row)->setValue('Costo Niño USD Total');
            $worksheet->getCellByColumnAndRow(16, $base_row)->setValue('Total USD');
            $worksheet->getCellByColumnAndRow(17, $base_row)->setValue('Gran Total');
            $worksheet->getCellByColumnAndRow(18, $base_row)->setValue('Porcentaje Utilidad');
            $worksheet->getCellByColumnAndRow(19, $base_row)->setValue('Utilidad Propia');
            $worksheet->getCellByColumnAndRow(20, $base_row)->setValue('Utilidad Agencia');

            /*
             * Se iteran cada una de las reservas.
             */
            foreach ($reservaciones as $key => $reserva) {
                if ($reserva->empresa === null) {
                    $reserva->empresa = 'Personal';
                }
                $row = $key + $base_row + 1;
                $worksheet->getCellByColumnAndRow(1, $row)->setValue($reserva->empresa);
                $excelDate = Date::PHPToExcel($reserva->fecha_venta);
                $worksheet->getCellByColumnAndRow(2, $row)->setValue($excelDate)
                    ->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                $excelDate = Date::PHPToExcel($reserva->fecha_reservacion);
                $worksheet->getCellByColumnAndRow(3, $row)->setValue($excelDate)
                    ->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                $worksheet->getCellByColumnAndRow(4, $row)->setValue($reserva->status);
                $worksheet->getCellByColumnAndRow(5, $row)->setValue($reserva->confirmacion);
                $worksheet->getCellByColumnAndRow(6, $row)->setValue($reserva->cliente);
                $worksheet->getCellByColumnAndRow(7, $row)->setValue($reserva->representante);
                $worksheet->getCellByColumnAndRow(8, $row)->setValue($reserva->tour . ' ' . $reserva->paquete);
                $worksheet->getCellByColumnAndRow(9, $row)->setValue($reserva->n_adultos); //H
                $worksheet->getCellByColumnAndRow(10, $row)->setValue($reserva->n_ninos); //I
                $worksheet->getCellByColumnAndRow(11, $row)->setValue($reserva->cambio_dolar) //J
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                $worksheet->getCellByColumnAndRow(12, $row)->setValue($reserva->precio_adulto) //K
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                $worksheet->getCellByColumnAndRow(13, $row)->setValue($reserva->precio_ninio) //L
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

                $adultos_totales = "=I$row*L$row";
                $ninos_totales = "=J$row*M$row";
                $dolares = "=N$row+O$row";
                $total = "=P$row*K$row";

                $worksheet->getCellByColumnAndRow(14, $row)->setValue($adultos_totales)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                $worksheet->getCellByColumnAndRow(15, $row)->setValue($ninos_totales)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                $worksheet->getCellByColumnAndRow(16, $row)->setValue($dolares)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                $worksheet->getCellByColumnAndRow(17, $row)->setValue($total)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

                $worksheet->getCellByColumnAndRow(18, $row)->setValue($reserva->utilidad / 100.0)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE_00);

                $utilidad = "=Q$row*R$row";
                $restante = "=Q$row-S$row";

                $worksheet->getCellByColumnAndRow(19, $row)->setValue($utilidad)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                $worksheet->getCellByColumnAndRow(20, $row)->setValue($restante)
                    ->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

            }

            /*
             * Se calcula el rango de datos.
             */
            $base = $worksheet->getCellByColumnAndRow(2, $base_row)->getCoordinate();
            $max = $worksheet->getCellByColumnAndRow(19, count($reservaciones) + $base_row)->getCoordinate();

            $rango = $base . ':' .$max;

            /*
             * Rango 'Reservas' con los datos de la tabla.
             */
            $spreadsheet->addNamedRange(new NamedRange('Reservas', $worksheet, $rango));

            $worksheet->getAutoFilter()->setRange($rango);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');

            $writer = new XlsxWriter($spreadsheet);
            $writer->save('php://output');
        } catch (\Exception $e) {
            throw $e;
            //return view('vendor/voyager/reportes/error');
        }
        die();
    }
}
