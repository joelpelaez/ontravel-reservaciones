<?php

namespace App\Http\Controllers;

use App\Agencias;
use App\Cupones;
use App\HistorialReservaciones;
use App\Hoteles;
use App\Idiomas;
use App\IdiomasTours;
use App\Motivos;
use App\MotivosStatus;
use App\Nacionalidades;
use App\PaquetesTours;
use App\PickupsHoteles;
use App\RegionesTours;
use App\Representantes;
use App\Reservaciones;
use App\StatusesReservaciones;
use App\TiposCupones;
use App\Tours;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ReservacionesController extends Controller
{

    public function index()
    {

    }

    public function reps($id)
    {
        $reps = Representantes::getReps($id);
        return $reps;
    }

    public function tours($id)
    {
        $tours = Tours::getTours($id);
        return $tours;
    }

    public function paquetestours($id)
    {
        $paquetestours = PaquetesTours::paquetes($id);
        return $paquetestours;
    }

    public function areahoteles($id_hotel)
    {
        $areahoteles = PickupsHoteles::getAreaHoteles($id_hotel);
        return $areahoteles;
    }

    public function pickupshoteles($id, $area_hotel)
    {
        $pickupshoteles = PickupsHoteles::getPickupsHoteles($id, $area_hotel);
        return $pickupshoteles;
    }

    public function idiomastours($id)
    {
        $getidiomastours = Idiomas::getIdiomasTours($id);
        return $getidiomastours;
    }

    public function hoteles($id)
    {
        $hoteles = Hoteles::getHotelesByIdRegion($id);
        return $hoteles;
    }

    public function regiontour($id)
    {
        $regiontour = RegionesTours::getRegionesByIdTour($id);
        return $regiontour;
    }

    public function create()
    {
        $agencias = Agencias::getAgencias();
        $tiposcupones = TiposCupones::getTiposCupones();
        $hoteles = Hoteles::getHoteles();
        $nacionalidades = Nacionalidades::getNacionalidades();
        $tours = Tours::getToursActivos();
        return view('/vendor/voyager/reservaciones/reservar', compact(
            'agencias', 'hoteles', 'nacionalidades', 'tiposcupones', 'tours'));
    }

    /**
     * Almacena las nuevas reservas
     *
     * Crea registros de las nuevas reservaciones, con la información de los cupones y
     * los códigos de confirmación correspondientes.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse Retorna la vista a mostrar
     * @throws \Exception Relanza la excepción si se encontró un problema en la transacción.
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            $reserva = new Reservaciones;

            /*
             * Si el hotel no es seleccionado se marca como -1 y se toma
             * en cuenta la información extra de dirección y pickup personalizado.
             */
            if ($request->id_hotel === "-1") {
                $reserva->id_hotel = null;
                $reserva->direccion = $request->direccion;
                $reserva->id_pickup_hotel = null;
                $reserva->pickup_hora = $request->pickup_hora;
            } else {
                $reserva->id_hotel = $request->id_hotel;
                $reserva->id_pickup_hotel = $request->pickup;
                $reserva->direccion = null;
                $reserva->pickup_hora = null;
            }

            /*
             * Si existe el campo de habitación se registra, en caso contrario
             * se marca como "N/A".
             */
            if (!empty($request->habitacion)) {
                $reserva->habitacion = $request->habitacion;
            } else {
                $reserva->habitacion = "N/A";
            }

            $reserva->apellido = $request->apellido;
            $reserva->id_tour = $request->id_tour;
            $reserva->id_paquete_tour = $request->id_paquete_tour;

            if ($request->id_agencia !== "-1")
                $reserva->id_agencia = $request->id_agencia;

            /*
             * Si se define un representante nuevo, se registra al momento antes de
             * proceder con la reservación. Sino es así se selecciona uno existente.
             * Si se elige la opción "Sin representante", se manda el valor -1 y se
             * interpreta como null.
             */
            if (!empty($request->rep_name)) {
                $representante = new Representantes();
                $representante->id_agencia = $request->id_agencia;
                $representante->activo = true;
                $representante->nombre = $request->rep_name;
                $representante->save();
                $reserva->representante()->associate($representante);
            } else if ($request->id_rep !== "-1") {
                $reserva->id_representante = $request->id_rep;
            } else {
                $reserva->id_representante = null;
            }

            $reserva->id_idioma = $request->id_idioma;
            $reserva->id_nacionalidad = $request->id_nacionalidad;
            $reserva->fecha_reservacion = $request->fecha_reserva;
            $reserva->fecha_venta = Carbon::today()->toDateString();
            $reserva->go_show = $request->go_show;
            $reserva->id_user = Auth::user()->id;

            if ($request->go_show)
                $reserva->id_status_reservacion = 4;
            else
                $reserva->id_status_reservacion = 1;

            $reserva->save();

            $id_reservacion = $reserva->id;
            $confirmacion = null;
            /*
             * Se registran cada uno de los cupones de la reservación
             */
            if (!empty($id_reservacion)) {
                $tipos = $request->tipos;

                foreach ($tipos as $key => $tipo) {
                    $cupones = new Cupones;
                    $cupones->id_reservacion = $id_reservacion;
                    $cupones->id_tipo_cupon = $tipo;
                    $cupones->n_adultos = $request->n_adultos[$key];
                    $cupones->n_ninos = $request->n_ninos[$key];
                    $cupones->n_infantes = $request->n_infantes[$key];
                    $cupones->cupon = $request->cupones[$key];
                    $cupones->observaciones = $request->observaciones[$key];
                    $cupones->confirmacion = $request->confirmacion_cupon[$key];
                    $cupones->save();

                    if (empty($cupones->confirmacion)) {
                        $confirmacion_cupon = self::getCodigoConfirmacion($request, $id_reservacion);

                        $cupones->newQuery()->where('id', '=', $cupones->id)
                            ->update(['confirmacion' => $confirmacion_cupon]);
                        $confirmacion = $confirmacion_cupon;
                    }
                }
            }

            // Crea un codigo de confirmación si no se definió uno anteriormente.
            if (!empty($request->confirmacion)) {
                $confirmacion = $request->confirmacion;
            } else if ($confirmacion === null) {
                $confirmacion = self::getCodigoConfirmacion($request, $id_reservacion);
            }

            $reserva->newQuery()
                ->where('id', '=', $id_reservacion)
                ->update(['confirmacion' => $confirmacion]);

            // Se guarda el historial de actividad del usuario
            $id_user = Auth::user()->id;
            $estado = $reserva->statusReservacion->nombre;
            HistorialReservaciones::registrar($id_user, $id_reservacion, $estado, null);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        return redirect()->route('reservaciones.show', ['id' => $id_reservacion]);
    }

    public function show($id)
    {
        $reservacion = Reservaciones::find($id);
        $cupones = Cupones::query()->where('id_reservacion', '=', $id)->get();
        $id_cupon = $cupones[0]['id'];
        $cupon = Cupones::find($id_cupon);
        $historico = HistorialReservaciones::obtenerPorReservacion($id);

        return view('/vendor/voyager/reservaciones/mostrar', compact('reservacion', 'cupon', 'cupones', 'historico'));
    }

    public function parametros()
    {

        $reservas = Reservaciones::getReservaciones();
        $agencias = Agencias::getAgencias();
        $hoteles = Hoteles::getHoteles();
        $reps = Representantes::getReps($id_representante = "");
        $tours = Tours::getToursActivos();


        return view('/vendor/voyager/reservaciones/buscar', compact('agencias', 'hoteles', 'reps', 'tours'));
    }

    public function resultados(Request $request)
    {
        $reservas = Reservaciones::obtenerReservaciones($request);
        $cupones = Cupones::getCupones();
        $motivos = Motivos::getMotivos();

        return view('/vendor/voyager/reservaciones/resultados', compact('reservas', 'cupones', 'motivos', 'request'));
    }


    public function edit($id)
    {
        $reservacion = Reservaciones::find($id);
        $cupones = Cupones::query()->where('id_reservacion', '=', $id)->get();
        $agencias = Agencias::getAgencias();
        $hoteles = Hoteles::getHoteles();
        $tiposcupones = TiposCupones::getTiposCupones();
        $nacionalidades = Nacionalidades::getNacionalidades();
        $representantes = self::reps($reservacion->id_agencia);
        $reservacion = Reservaciones::find($id);
        $tours = Tours::getToursActivos();

        //$pickupshoteles = PickupsHoteles::getPickupsHotelesByHotel($reservacion->id_hotel);
        $pickupshoteles = PickupsHoteles::getAreaHoteles($reservacion->id_hotel);

        if ($reservacion->pickup !== null)
            $pickupsarea = PickupsHoteles::getPickupsHoteles($reservacion->id_hotel, $reservacion->pickup->area_pickup);
        else
            $pickupsarea = [];

        $idiomasTour = IdiomasTours::getIdiomasTourByIdTour($reservacion->id_tour);
        $paquetestours = PaquetesTours::getPaquetesTourByIdTour($reservacion->id_tour);
        $fecha_reservacion = $reservacion->fecha_reservacion->format('Y-m-d');
        $motivos = Motivos::getMotivos();

        // Prepare some data for fields
        if ($reservacion->hotel === null)
            $reservacion->id_hotel = -1;
        if ($reservacion->agencia === null)
            $reservacion->id_agencia = -1;
        if ($reservacion->representante === null)
            $reservacion->id_representante = -1;

        return view('/vendor/voyager/reservaciones/editar', compact('agencias'
            , 'representantes', 'hoteles', 'tours', 'nacionalidades', 'tiposcupones', 'reservacion', 'cupones',
            'pickupshoteles', 'idiomasTour', 'paquetestours', 'fecha_reservacion', 'motivos', 'pickupsarea'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $reserva = Reservaciones::query()->find($id);

            /*
             * Si el hotel no es seleccionado se marca como -1 y se toma
             * en cuenta la información extra de dirección y pickup personalizado.
             */
            if ($request->id_hotel === "-1") {
                $reserva->id_hotel = null;
                $reserva->direccion = $request->direccion;
                $reserva->id_pickup_hotel = null;
                $reserva->pickup_hora = $request->pickup_hora;
            } else {
                $reserva->id_hotel = $request->id_hotel;
                $reserva->id_pickup_hotel = $request->id_pickup_hotel;
                $reserva->direccion = null;
                $reserva->pickup_hora = null;
            }

            /*
             * Si existe el campo de habitación se registra, en caso contrario
             * se marca como "N/A".
             */
            if (!empty($request->habitacion)) {
                $reserva->habitacion = $request->habitacion;
            } else {
                $reserva->habitacion = "N/A";
            }

            $reserva->apellido = $request->apellido;
            $reserva->id_tour = $request->id_tour;
            $reserva->id_paquete_tour = $request->id_paquete_tour;

            if ($request->id_agencia !== "-1")
                $reserva->id_agencia = $request->id_agencia;
            else
                $reserva->id_agencia = null;

            /*
             * Si se define un representante nuevo, se registra al momento antes de
             * proceder con la reservación. Sino es así se selecciona uno existente.
             * Si se elige la opción "Sin representante", se manda el valor -1 y se
             * interpreta como null.
             */
            if (!empty($request->rep_name)) {
                $representante = new Representantes();
                $representante->id_agencia = $request->id_agencia;
                $representante->activo = true;
                $representante->nombre = $request->rep_name;
                $representante->save();
                $reserva->representante()->associate($representante);
            } else if ($request->id_rep !== "-1") {
                $reserva->id_representante = $request->id_rep;
            } else {
                $reserva->id_representante = null;
            }

            $reserva->id_idioma = $request->id_idioma;
            $reserva->id_nacionalidad = $request->id_nacionalidad;
            $reserva->fecha_reservacion = $request->fecha_reserva;
            $reserva->go_show = $request->go_show;
            $reserva->save();

            $ids = $request->cupon_id;

            foreach ($ids as $key => $_) {
                if ($_ !== "-1") {
                    Cupones::query()
                        ->where('id', '=', $request->cupon_id[$key])
                        ->update(['id_tipo_cupon' => $request->tipos[$key],
                            'n_adultos' => $request->n_adultos[$key],
                            'n_ninos' => $request->n_ninos[$key],
                            'cupon' => $request->cupones[$key],
                            'observaciones' => $request->observaciones[$key]]);
                } else {
                    $cupon = new Cupones();
                    $cupon->id_reservacion = $reserva->id;
                    $cupon->id_tipo_cupon = $request->tipos[$key];
                    $cupon->n_adultos = $request->n_adultos[$key];
                    $cupon->n_ninos = $request->n_ninos[$key];
                    $cupon->n_infantes = $request->n_infantes[$key];
                    $cupon->cupon = $request->cupones[$key];
                    $cupon->observaciones = $request->observaciones[$key];
                    $cupon->save();
                }
            }

            $id_user = Auth::user()->id;
            HistorialReservaciones::registrar($id_user, $reserva->id, $reserva->statusReservacion->nombre, null, true);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        return redirect()->route('reservaciones.show', $id);
    }

    public function goShow(Request $request, $id) {
        $reservacion = Reservaciones::query()->find($id);

        if ($reservacion->id_status_reservacion !== 3) {
            $estadoAnterior = $reservacion->statusReservacion->nombre;
            $reservacion->id_status_reservacion = 4;
            $reservacion->save();

            HistorialReservaciones::registrar(Auth::user()->id, $reservacion->id,
                StatusesReservaciones::find($reservacion->id_status_reservacion)->nombre,
                $estadoAnterior, true);
        }

        if ($request->exists('fast'))
            return new Response("Go Show");

        return redirect()->route('reservaciones.show', $id);
    }

    public function noShow(Request $request, $id) {
        $reservacion = Reservaciones::query()->find($id);

        if ($reservacion->id_status_reservacion !== 3) {
            $estadoAnterior = $reservacion->statusReservacion->nombre;
            $reservacion->id_status_reservacion = 5;
            $reservacion->save();


            HistorialReservaciones::registrar(Auth::user()->id, $reservacion->id,
                StatusesReservaciones::find($reservacion->id_status_reservacion)->nombre,
                $estadoAnterior, true);
        }

        if ($request->exists('fast'))
            return new Response("No Show");

        return redirect()->route('reservaciones.show', $id);
    }

    public function efectivo(Request $request, $id) {
        $reservacion = Reservaciones::query()->find($id);

        if ($reservacion->id_status_reservacion !== 3) {
            $estadoAnterior = $reservacion->statusReservacion->nombre;
            $reservacion->id_status_reservacion = 6;
            $reservacion->save();

            HistorialReservaciones::registrar(Auth::user()->id, $reservacion->id,
                StatusesReservaciones::find($reservacion->id_status_reservacion)->nombre,
                $estadoAnterior, true);
        }

        if ($request->exists('fast'))
            return new Response("Efectivo");

        return redirect()->route('reservaciones.show', $id);
    }

    public function cancelar(Request $request, $id)
    {
        $reservacion = Reservaciones::find($id);

        if ($reservacion->id_status_reservacion != 3) {
            $estadoAnterior = $reservacion->statusReservacion->nombre;
            Reservaciones::where('id', '=', $id)
                ->update(['id_status_reservacion' => 3,
                    'fecha_modificacion_status' => date("Y-m-d")]);

            $motivosSts = new MotivosStatus;
            $motivosSts->id_reservacion = $id;
            $motivosSts->id_motivo = $request->motivos;
            $motivosSts->id_status = 3;
            $motivosSts->save();

            HistorialReservaciones::registrar(Auth::user()->id, $reservacion->id,
                StatusesReservaciones::find(3)->nombre,
                $estadoAnterior, true);
        }
        return redirect()->route('reservaciones.show', $id);
    }

    public function reagendar(Request $request, $id)
    {
        $reservacion = Reservaciones::find($id);
        $fecha_reserva = date_format($reservacion->fecha_reservacion, "Y-m-d");

        if ($reservacion->id_status_reservacion != 2 AND $request->fecha_reserva != $fecha_reserva) {
            $estadoAnterior = $reservacion->statusReservacion->nombre;
            Reservaciones::where('id', '=', $id)
                ->update([
                    'id_status_reservacion' => 2,
                    'fecha_modificacion_status' => date("Y-m-d"),
                    'fecha_reservacion' => $request->fecha_reservacion
                ]);

            $motivosSts = new MotivosStatus;
            $motivosSts->id_reservacion = $id;
            $motivosSts->id_motivo = $request->motivos;
            $motivosSts->id_status = 2;
            $motivosSts->save();

            HistorialReservaciones::registrar(Auth::user()->id, $reservacion->id,
                StatusesReservaciones::find(2)->nombre,
                $estadoAnterior, true);
        }
        return redirect()->route('reservaciones.show', $id);
    }

    private function getCodigoConfirmacion(Request $request, $id_reservacion)
    {

        $codigo = Tours::getCodigoById($request->id_tour);
        $codigo = $codigo[0]['codigo'];

        $idioma = Idiomas::query()->whereHas('idiomaTour', function ($q) use ($request, $id_reservacion) {
            $q->where('id', '=', $request->id_idioma);
        })->first();
        $idioma = substr($idioma->nombre, 0, 1);

        $fecha = $request->fecha_reserva;
        $dia = substr($fecha, -2);
        $mes = substr($fecha, 5, -3);
        $user = Auth::user()->name;
        $user = substr($user, 0, 1);

        $cupones = Cupones::query()->whereHas('reserva', function (Builder $q) use ($request, $id_reservacion) {
            $q->where('fecha_reservacion', '=', $request->fecha_reserva);
            $q->where('id_tour', '=', $request->id_tour);
        })->get();

        $adultos = 0;

        foreach ($cupones as $cupon) {
            $adultos += $cupon->n_adultos;
        }

        $confirmacion = $codigo . $dia . $mes . $idioma . $user . $adultos;
        return $confirmacion;
    }

}