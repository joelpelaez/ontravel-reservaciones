<?php

namespace App\Http\Controllers;


use App\Agencias;
use App\PaquetesTours;
use App\PrecioTour;
use App\Tours;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrecioTourController extends Controller {

    public function index() {
        $agencias = Agencias::all();
        $tours = Tours::all();

        return view('vendor/voyager/precio/index', compact('agencias', 'tours'));
    }

    public function mostrarRedirect(Request $request) {
        return redirect()->route('precio.mostrar',
            ['agencia' => $request->agencia_id, 'paquete_tour' => $request->paquete_tour_id]
        );
    }

    public function mostrar(Request $request, $agencia_id, $paquete_tour_id) {
        $agencia = Agencias::query()->find($agencia_id);
        $paqueteTour = PaquetesTours::query()->find($paquete_tour_id);
        $precios = PrecioTour::query()
            ->where('agencia_id', '=', $agencia_id)
            ->where('paquete_tour_id', '=', $paquete_tour_id)
            ->get();

        return view('vendor/voyager/precio/mostrar', compact('agencia', 'precios', 'paqueteTour'));
    }

    /**
     * Se encarga de realizar las tareas de
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function actualizar(Request $request) {
        $fecha = Carbon::today();
        $anterior = $fecha->copy()->subDay();
        $precio_adulto = $request->precio_adulto;
        $precio_ninio = $request->precio_ninio;
        $porcentaje_utilidad = $request->porcentaje_utilidad;
        $agencia = $request->agencia_id;
        $paquete = $request->paquete_tour_id;

        $validation = [
            'precio_adulto' => 'required|numeric',
            'precio_ninio' => 'required|numeric',
            'porcentaje_utilidad' => 'required|numeric'
        ];

        $messages = [
            'precio_adulto.numeric' => 'El Precio para Adulto debe ser un número',
            'precio_adulto.required' => 'El Precio para Adulto es obligatorio',
            'precio_ninio.numeric' => 'El Precio para Niño debe ser un número',
            'precio_ninio.required' => 'El Precio para Niño es obligatorio',
            'porcentaje_utilidad.numeric' => 'El Porcentaje de Utilidad debe ser un número',
            'porcentaje_utilidad.required' => 'El Porcentaje de Utilidad es obligatorio'
        ];

        $this->validate($request, $validation, $messages);

        DB::beginTransaction();
        try {

            $tipoCambioDeHoy = PrecioTour::query()
                ->where('agencia_id', '=', $agencia)
                ->where('paquete_tour_id', '=', $paquete)
                ->whereDate('fecha_inicio', '=', $fecha)
                ->get();

            /*
             * Si el precio del tour de hoy se cambia, afecta al mismo dia, si ya se ha realizado
             * antes la actualización del precio.
             */
            if ($tipoCambioDeHoy->count() > 0) {
                $tipoCambio = $tipoCambioDeHoy->first();
                $tipoCambio->precio_adulto = $precio_adulto;
                $tipoCambio->precio_ninio = $precio_ninio;
                $tipoCambio->porcentaje_utilidad = $porcentaje_utilidad;
                $tipoCambio->save();
            } else {
                /*
                 * Se comprueba que no existan precios en el "futuro"
                 */
                $res = PrecioTour::query()
                    ->where('agencia_id', '=', $agencia)
                    ->where('paquete_tour_id', '=', $paquete)
                    ->whereDate('fecha_inicio', '>', $fecha)
                    ->count();
                if ($res > 0) {
                    /*
                     * Se realizara un rollback y se regresara a la pantalla anterior.
                     */
                    DB::rollback();

                    return redirect()->route('tipocambio.mostrar', ['id' => $agencia]);
                }
                /*
                 * Si no aplica, entonces se registra un nuevo precio para el periodo actual.
                 * Primero revisando cual es el actual e invalidarlo.
                 */
                $tipoCambioActual = PrecioTour::query()
                    ->where('agencia_id', '=', $agencia)
                    ->where('paquete_tour_id', '=', $paquete)
                    ->where('activo', '=', 1)
                    ->whereNull('fecha_fin')
                    ->get();

                /*
                 * Si existe un precio activo, se marca como inactivo y la fecha de fin
                 * pasa a ser el día anterior (ayer).
                 */
                if ($tipoCambioActual->count() === 1) {
                    $tipoCambioAnterior = $tipoCambioActual->first();
                    $tipoCambioAnterior->activo = false;
                    $tipoCambioAnterior->fecha_fin = $anterior;
                    $tipoCambioAnterior->save();
                }

                /*
                 * Se crea nueva instancia del precio del tour para la agencia.
                 */
                $tipoCambioNuevo = new PrecioTour();
                $tipoCambioNuevo->agencia_id = $agencia;
                $tipoCambioNuevo->paquete_tour_id = $paquete;
                $tipoCambioNuevo->precio_adulto = $precio_adulto;
                $tipoCambioNuevo->precio_ninio = $precio_ninio;
                $tipoCambioNuevo->porcentaje_utilidad = $porcentaje_utilidad;
                $tipoCambioNuevo->fecha_inicio = $fecha;
                $tipoCambioNuevo->fecha_fin = null;
                $tipoCambioNuevo->activo = true;
                $tipoCambioNuevo->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            /*
             * Rethrow exception for debugging proposes.
             */
            throw $e;
        }

        DB::commit();

        return redirect()->route('precio.mostrar', ['agencia_id' => $agencia, 'paquete_tour_id' => $paquete]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editar(Request $request, $id) {
        $precioTour = PrecioTour::query()->find($id);

        $anterior = PrecioTour::query()
            ->where('agencia_id', '=', $precioTour->agencia_id)
            ->where('paquete_tour_id', '=', $precioTour->paquete_tour_id)
            ->whereDate('fecha_fin', '=', Carbon::parse($precioTour->fecha_inicio)->subDay())
            ->first();

        $posterior = PrecioTour::query()
            ->where('agencia_id', '=', $precioTour->agencia_id)
            ->where('paquete_tour_id', '=', $precioTour->paquete_tour_id)
            ->whereDate('fecha_inicio', '=', Carbon::parse($precioTour->fecha_fin)->addDay())
            ->first();

        $fecha_inicio_min = null;
        $fecha_fin_max = null;
        $enable_fecha_fin = $precioTour->fecha_fin !== null;

        if ($anterior !== null) {
            $fecha_inicio_min = Carbon::parse($anterior->fecha_inicio)->addDay()->toDateString();
        }

        if ($posterior !== null) {
            $fecha_fin_max = Carbon::parse($posterior->fecha_fin)->subDay()->toDateString();
        }

        $precio_adulto = $precioTour->precio_adulto;
        $precio_ninio = $precioTour->precio_ninio;
        $fecha_inicio = $precioTour->fecha_inicio;
        $fecha_fin = $precioTour->fecha_fin;
        $porcentaje_utilidad = $precioTour->porcentaje_utilidad;

        return view('vendor/voyager/precio/editar',
            compact('id', 'precio_adulto', 'precio_ninio', 'fecha_inicio', 'fecha_fin',
                'enable_fecha_fin', 'fecha_fin_max', 'fecha_inicio_min', 'porcentaje_utilidad'));
    }

    public function actualizarAnterior(Request $request) {
        $id = $request->id;
        $precio_adulto = $request->precio_adulto;
        $precio_ninio = $request->precio_ninio;
        $fecha_inicio = $request->fecha_inicio;
        $fecha_fin = $request->fecha_fin;
        $porcentaje_utilidad = $request->porcentaje_utilidad;

        /*
         * Se evaluará que valores han cambiado para proceder a actualizarlos.
         */
        $precioTour = PrecioTour::query()->find($id);
        $cambioAnterior = null;
        $cambioSiguiente = null;

        $validation = [
            'precio_adulto' => 'required|numeric',
            'precio_ninio' => 'required|numeric',
            'porcentaje_utilidad' => 'required|numeric'
        ];

        $messages = [
            'precio_adulto.numeric' => 'El Precio para Adulto debe ser un número',
            'precio_adulto.required' => 'El Precio para Adulto es obligatorio',
            'precio_ninio.numeric' => 'El Precio para Niño debe ser un número',
            'precio_ninio.required' => 'El Precio para Niño es obligatorio',
            'porcentaje_utilidad.numeric' => 'El Porcentaje de Utilidad debe ser un número',
            'porcentaje_utilidad.required' => 'El Porcentaje de Utilidad es obligatorio'
        ];

        if ($fecha_inicio !== $precioTour->fecha_inicio) {
            /*
             * Se buscara si existe algun registro anterior a este para actualizarlo en conjunto.
             */
            $fechaAnterior = Carbon::parse($precioTour->fecha_inicio)->subDay();
            $cambioAnterior = PrecioTour::query()
                ->where('agencia_id', '=', $precioTour->agencia_id)
                ->where('paquete_tour_id', '=', $precioTour->paquete_tour_id)
                ->whereDate('fecha_fin', '=', $fechaAnterior)
                ->first();

            if ($cambioAnterior !== null) {
                $fecha = date_create($cambioAnterior->fecha_inicio);
                if ($precioTour->fecha_fin !== null)
                    $validation['fecha_inicio'] = "required|date|before_or_equal:fecha_fin|after_or_equal:{$cambioAnterior->fecha_inicio}";
                else
                    $validation['fecha_inicio'] = "required|date|before_or_equal:today|after_or_equal:{$cambioAnterior->fecha_inicio}";
                $messages['fecha_inicio.after_or_equal'] = "La Fecha Inicio del periodo debe ser mayor a {$fecha->format('d/m/Y')}";
            } else if ($precioTour->fecha_fin !== null) {
                $validation['fecha_inicio'] = "required|date|before_or_equal:fecha_fin";
            } else {
                $validation['fecha_inicio'] = "required|date|before_or_equal:today";
            }

            $messages['fecha_inicio.required'] = "La Fecha Inicio es obligatoria";
            $messages['fecha_inicio.before_or_equal'] = "La Fecha Inicio debe ser menor o igual a Fecha Fin";
        }

        if ($precioTour->fecha_fin !== null && $fecha_fin !== $precioTour->fecha_fin) {
            /*
             * Se buscara si existe algun registro posterior a este para actualizarlo en conjunto.
             */
            $fechaPosterior = Carbon::parse($precioTour->fecha_fin)->addDay();
            $cambioSiguiente = PrecioTour::query()
                ->where('agencia_id', '=', $precioTour->agencia_id)
                ->where('paquete_tour_id', '=', $precioTour->paquete_tour_id)
                ->whereDate('fecha_inicio', '=', $fechaPosterior)
                ->first();

            if ($cambioSiguiente !== null) {
                if ($cambioSiguiente->fecha_fin !== null) {
                    $fecha = date_create($cambioSiguiente->fecha_fin);
                    $validation['fecha_fin'] = "required|date|after_or_equal:fecha_inicio|before_or_equal:{$cambioSiguiente->fecha_fin}";
                    $messages['fecha_fin.before_or_equal'] = "La Fecha Fin del periodo debe ser menor o igual a {$fecha->format('d/m/Y')}";
                } else {
                    $ayer = Carbon::today()->subDay();
                    $validation['fecha_fin'] = "required|date|after_or_equal:fecha_inicio|before_or_equal:{$ayer->toDateString()}";
                    $messages['fecha_fin.before_or_equal'] = "La Fecha Fin del periodo debe ser menor o igual a {$ayer->format('d/m/Y')}";
                }
            } else {
                $validation['fecha_fin'] = "required|date|after_or_equal:fecha_inicio";
            }
            $messages['fecha_fin.required'] = "La Fecha Fin es obligatoria";
            $messages['fecha_fin.after_or_equal'] = "La Fecha Fin debe ser mayor o igual a la Fecha Inicio";
        }

        $this->validate($request, $validation, $messages);

        /*   z<
         * Se inicia el proceso de actualización.
         */
        DB::beginTransaction();

        try {
            if ($cambioAnterior !== null) {
                $fechaAnteriorNueva = Carbon::parse($fecha_inicio)->subDay();
                $cambioAnterior->fecha_fin = $fechaAnteriorNueva;
                $cambioAnterior->save();
            }

            if ($cambioSiguiente !== null) {
                $fechaPosteriorNueva = Carbon::parse($fecha_fin)->addDay();
                $cambioSiguiente->fecha_inicio = $fechaPosteriorNueva;
                $cambioSiguiente->save();
            }

            $precioTour->fecha_inicio = $fecha_inicio;
            $precioTour->fecha_fin = $fecha_fin;
            $precioTour->precio_adulto = $precio_adulto;
            $precioTour->precio_ninio = $precio_ninio;
            $precioTour->porcentaje_utilidad = $porcentaje_utilidad;
            $precioTour->save();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();


        return redirect()->route('precio.mostrar', ['agencia_id' => $precioTour->agencia_id, 'paquete_tour_id' => $precioTour->paquete_tour_id]);
    }
}