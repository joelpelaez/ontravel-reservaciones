<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusesReservaciones extends Model
{
    public function motivosStatus()
    {
        return $this->hasMany('App\MotivosStatus', 'id_status_reservacion');
    }
}
