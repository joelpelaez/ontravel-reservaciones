<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Agencias extends Model {
    public static function getAgencias() {
        $agencias = self::where('activo', 1)
            ->orderBy('nombre', 'Asc')
            ->get();

        return $agencias;
    }

    public function reservacion() {
        return $this->hasMany('App\Reservaciones');
    }

    public function tiposCambio() {
        return $this->hasMany(TipoCambio::class);
    }

    public function preciosTour() {
        return $this->hasMany(PrecioTour::class);
    }
}
