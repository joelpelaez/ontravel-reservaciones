<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialReservaciones extends Model
{
    protected $table = 'historial_reservaciones';

    protected $fillable = ["id", "id_usuario", "id_reservacion", "estado_actual", "estado_anterior",
        "fecha_historial", "created_at", "updated_at"];

    protected $dates = ['fecha_historial', 'created_at', 'updated_at'];

    public function user() {
        return $this->belongsTo(User::class, 'id_usuario');
    }

    public function reservacion() {
        return $this->belongsTo(Reservaciones::class, 'id_reservacion');
    }

    public static function registrar($user_id, $reservacion_id, $estado_actual, $estado_anterior, $update = false) {
        $registro = new HistorialReservaciones();
        $registro->id_usuario = $user_id;
        $registro->id_reservacion = $reservacion_id;
        $registro->estado_actual = $estado_actual;
        $registro->estado_anterior = $estado_anterior;
        $registro->accion = $update ? "Actualizado" : "Creado";
        $registro->save();
    }

    public static function obtenerPorReservacion($reservacion_id) {
        $registros = HistorialReservaciones::query()
            ->where('id_reservacion', '=', $reservacion_id)
            ->get();

        return $registros;
    }

    public static function obtenerPorUsuario($user_id) {
        $registros = HistorialReservaciones::query()
            ->where('id_user', '=', $user_id)
            ->get();

        return $registros;
    }
}
