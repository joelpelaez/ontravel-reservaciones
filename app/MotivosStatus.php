<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivosStatus extends Model
{
    protected $table = 'motivos_statuses';
    protected $fillable = ['id','id_motivo','id_status'];
    
	public static function getMotivosStatus(){
    	$motivosStatus = self::orderBy('id','Asc')
    	->get();

    	return $motivosStatus;
    }

    public  function motivo(){
    	return $this->belongsTo('App\Motivos','id_motivo');
    }

    public  function statusRerserva(){
    	return $this->belongsTo('App\StatusesReservaciones','id_status');
    }
}
