<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

class Reservaciones extends Model
{
    protected $table = 'reservaciones';

    protected $fillable = ['id', 'id_hotel', 'direccion', 'apellido', 'habitacion', 'id_tour', 'id_paquete_tour', 'id_agencia', 'id_representante', 'id_idioma', 'id_nacionalidad', 'id_status_reservacion', 'fecha_reservacion', 'fecha_venta', 'fecha_modificacion_status', 'confirmacion', 'go_show', 'no_show', 'id_pickup_hotel', 'pickup_hora'];

    protected $dates = ['fecha_reservacion', 'fecha_venta'];

    public static function obtenerReservaciones($request)
    {
        $resultados = Reservaciones::query();

        if (!empty($request->id_hotel)) {
            if ($request->id_hotel === "-1")
                $resultados->whereNull('id_hotel');
            else
                $resultados->where('id_hotel', '=', $request->id_hotel);
        }
        if (!empty($request->id_tour)) {
            $resultados->where('id_tour', '=', $request->id_tour);
        }
        if (!empty($request->id_paquete)) {
            $resultados->where('id_paquete_tour', '=', $request->id_paquete);
        }
        if (!empty($request->apellido)) {
            $resultados->where('apellido', '=', $request->apellido);
        }
        if (!empty($request->id_agencia)) {
            if ($request->id_agencia === "-1")
                $resultados->whereNull('id_agencia');
            else
                $resultados->where('id_agencia', '=', $request->id_agencia);
        }
        if (!empty($request->id_representante)) {
            if ($request->id_representante === "-1")
                $resultados->whereNull('id_representante');
            else
                $resultados->where('id_representante', '=', $request->id_representante);
        }

        // Busqueda por fecha de venta
        if (!empty($request->fecha_venta)) {
            // Solo un día en especifico
            $resultados->where('fecha_venta', '=', $request->fecha_venta);
        } else if (!empty($request->fecha_venta_inicio) && !empty($request->fecha_venta_fin)) {
            // Rango de dos fechas
            $resultados->whereBetween('fecha_venta', [$request->fecha_venta_inicio, $request->fecha_venta_fin]);
        } else if (!empty($request->fecha_venta_inicio)) {
            // Despues de la fecha
            $resultados->where('fecha_venta', '>=', $request->fecha_venta_inicio);
        } else if (!empty($request->fecha_venta_fin)) {
            // Antes de la fecha
            $resultados->where('fecha_venta', '<=', $request->fecha_venta_fin);
        }

        // Busqueda por fecha de reservacion
        if (!empty($request->fecha_reservacion)) {
            // Solo un día en especifico
            $resultados->where('fecha_reservacion', '=', $request->fecha_reservacion);
        } else if (!empty($request->fecha_inicio) && !empty($request->fecha_fin)) {
            // Rango de dos fechas
            $resultados->whereBetween('fecha_reservacion', [$request->fecha_inicio, $request->fecha_fin]);
        } else if (!empty($request->fecha_inicio)) {
            // Despues de la fecha
            $resultados->where('fecha_reservacion', '>=', $request->fecha_inicio);
        } else if (!empty($request->fecha_fin)) {
            // Antes de la fecha
            $resultados->where('fecha_reservacion', '<=', $request->fecha_fin);
        }

        return $resultados->get();
    }

    public static function getReservaciones()
    {
        $reservas = self::query()->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesById($id)
    {
        $reservas = self::query()
            ->where('id', '=', $id)
            ->orderBy('id', 'Asc')->get();

        return $reservas;
    }

    public static function getReservacionesByApellido($apellido)
    {
        $reservas = self::
        where('apellido', '=', $apellido)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByFecha($fecha_reserva)
    {
        $reservas = self::
        where('fecha_reservacion', '=', $fecha_reserva)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByRangoFechas($fecha1, $fecha2)
    {
        $reservas = self::query()
            ->whereBetween('fecha_reservacion', [$fecha1, $fecha2])
            ->orderBy('fecha_reservacion', 'Asc')->get();
        return $reservas;
    }


    public static function getReservacionesByAgencia($id_agencia)
    {
        $reservas = self::query()
            ->where('id_agencia', '=', $id_agencia)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByTour($id_tour)
    {
        $reservas = self::query()
            ->where('id_tour', '=', $id_tour)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByPaquete($id_paquete)
    {
        $reservas = self::query()
            ->where('id_paquete_tour', '=', $id_paquete)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByHotel($id_hotel)
    {
        $reservas = self::query()
            ->where('id_hotel', '=', $id_hotel)
            ->orderBy('nombre', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByRep($id_rep)
    {
        $reservas = self::query()
            ->where('id_representante', '=', $id_rep)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByAgeRepTour($id_agencia, $id_rep, $id_tour)
    {
        $reservas = self::query()
            ->where('id_agencia', '=', $id_agencia)
            ->where('id_representante', '=', $id_rep)
            ->where('id_tour', '=', $id_tour)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByStatus($id_status)
    {
        $reservas = self::query()
            ->where('id_status_reservacion', '=', $id_status)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public static function getReservacionesByFechaByTour($fecha_reservacion, $id_tour)
    {
        $reservas = self::query()
            ->where('fecha_reservacion', '=', $fecha_reservacion)
            ->where('id_status_reservacion', '!=', 3)
            ->where('id_tour', '=', $id_tour)
            ->orderBy('id', 'Asc')->get();
        return $reservas;
    }

    public function hotel()
    {
        return $this->belongsTo('App\Hoteles', 'id_hotel');
    }

    public function paqueteTour()
    {
        return $this->belongsTo('App\PaquetesTours', 'id_paquete_tour');
    }

    public function idiomaTour()
    {
        return $this->belongsTo('App\IdiomasTours', 'id_idioma');
    }

    public function representante()
    {
        return $this->belongsTo('App\Representantes', 'id_representante');
    }

    public function agencia()
    {
        return $this->belongsTo('App\Agencias', 'id_agencia');
    }

    public function nacionalidad()
    {
        return $this->belongsTo('App\Nacionalidades', 'id_nacionalidad');
    }

    public function pickup()
    {
        return $this->belongsTo('App\PickupsHoteles', 'id_pickup_hotel');
    }

    public function cupon()
    {
        return $this->hasMany('App\Cupones', 'id_reservacion');
    }

    public function statusReservacion()
    {
        return $this->belongsTo('App\StatusesReservaciones', 'id_status_reservacion');
    }

    public function historialReservaciones()
    {
        return $this->hasMany('App\HistorialReservaciones', 'id_reservacion');
    }

    public function getFechaReservacionAttribute($date)
    {
        return new Date($date);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public static function obtenerReservacionesPorIdTourYPorRangoDeFechas($fecha1, $fecha2, $id_tour)
    {
        $reservas = self::query()
            ->where('id_tour', $id_tour)
            ->whereBetween('fecha_reservacion', [$fecha1, $fecha2])
            ->orderBy('fecha_reservacion', 'Asc')->get();
        return $reservas;
    }

    public static function obtenerReservacionesPorIdTourYPorRangoDeFechasYIdAgencia($fecha1, $fecha2, $id_tour, $id_agencia)
    {
        $reservas = self::query()
            ->orderBy('fecha_reservacion', 'Asc');
        if ($id_tour)
            $reservas->where('id_tour', $id_tour);
        if ($id_agencia)
            $reservas->where('id_agencia', $id_agencia);
        if ($fecha1)
            $reservas->where('fecha_venta', '>=', $fecha1);
        if ($fecha2)
            $reservas->where('fecha_venta', '<=', $fecha2);

        $reservas = $reservas->get();
        return $reservas;
    }

    /**
     * Get data from database
     * @param $fecha_inicio \DateTime Fecha de Venta (limite inferior)
     * @param $fecha_fin \DateTime Fecha de Venta (limite superior)
     * @param $agencia integer Identificador de la Agencia
     * @param $tour integer Identificador del Tour
     * @return \Illuminate\Support\Collection
     */
    public static function obtenerBuenoParaCobro($fecha_inicio, $fecha_fin, $agencia, $tour) {
        /**
         * @var \Illuminate\Database\Query\Builder $builder
         */
        $builder = DB::table('reservaciones');

        $builder
            ->join('statuses_reservaciones', 'reservaciones.id_status_reservacion', '=', 'statuses_reservaciones.id')
            ->join('cupones', 'reservaciones.id', '=', 'cupones.id_reservacion')
            ->leftJoin('agencias', 'reservaciones.id_agencia', '=', 'agencias.id')
            ->leftJoin('representantes', 'reservaciones.id_representante', '=', 'representantes.id')
            ->join('paquetes_tours', 'reservaciones.id_paquete_tour', '=', 'paquetes_tours.id')
            ->join('tours', 'paquetes_tours.id_tour', '=', 'tours.id')
            ->join('paquetes', 'paquetes_tours.id_paquete', '=', 'paquetes.id')
            ->leftJoin('tipo_cambios', function(JoinClause $builder) {
                $builder
                    ->on('reservaciones.id_agencia', '=', 'tipo_cambios.agencia_id')
                    ->on('reservaciones.fecha_venta', '>=', 'tipo_cambios.fecha_inicio')
                    ->on(function(JoinClause $builder) {
                        $builder
                            ->on('reservaciones.fecha_reservacion', '<=', 'tipo_cambios.fecha_fin')
                            ->orWhereNull('tipo_cambios.fecha_fin');
                    });
            })
            ->leftJoin('precio_tours', function(JoinClause $builder) {
                $builder
                    ->on('reservaciones.id_agencia', '=', 'precio_tours.agencia_id')
                    ->on('reservaciones.id_paquete_tour', '=', 'precio_tours.paquete_tour_id')
                    ->on('reservaciones.fecha_venta', '>=', 'precio_tours.fecha_inicio')
                    ->on(function(JoinClause $builder) {
                        $builder
                            ->on('reservaciones.fecha_venta', '<=', 'precio_tours.fecha_fin')
                            ->orWhereNull('precio_tours.fecha_fin');
                    });
            })
            ->select(
                [
                    'agencias.nombre as empresa',
                    'reservaciones.fecha_venta',
                    'reservaciones.fecha_reservacion',
                    'cupones.cupon',
                    'cupones.confirmacion',
                    'statuses_reservaciones.nombre as status',
                    'reservaciones.apellido as cliente',
                    'tours.nombre as tour',
                    'paquetes.nombre as paquete',
                    'representantes.nombre as representante',
                    'cupones.n_adultos',
                    'cupones.n_ninos',
                    'cupones.n_infantes',
                    'tipo_cambios.cambio as cambio_dolar',
                    'precio_tours.precio_adulto as precio_adulto',
                    'precio_tours.precio_ninio as precio_ninio',
                    'precio_tours.porcentaje_utilidad as utilidad'
                ]
            );

        if ($tour !== null)
            $builder->where('reservaciones.id_tour', '=', $tour);
        if ($agencia !== null)
            $builder->where('reservaciones.id_agencia', '=', $agencia);
        if ($fecha_inicio !== null)
            $builder->where('reservaciones.fecha_venta', '>=', $fecha_inicio);
        if ($fecha_fin !== null)
            $builder->where('reservaciones.fecha_venta', '<=', $fecha_fin);

        $results = $builder->get();

        return $results;
    }
}



