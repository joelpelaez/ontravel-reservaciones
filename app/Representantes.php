<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representantes extends Model
{
	protected $table = 'representantes';
    protected $fillable = ['id','nombre','id_agencia'];

    public static function getReps($id_agencia){
    	$reps = self::query()
    	    ->where('id_agencia', '=', $id_agencia )
    	    ->where('activo', '=', 1 )
    	    ->orderBy('nombre','Asc')->get();

      	return $reps;
    }

    public function reservacion()
    {
        return $this->hasMany('App\Reservaciones', 'id_representante', 'local_key'); 
    }
}