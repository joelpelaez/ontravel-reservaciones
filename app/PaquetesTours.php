<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaquetesTours extends Model
{

    protected $table = 'paquetes_tours';
    protected $fillable = ['id', 'id_paquete', 'id_tour'];

    public static function paquetes($id_tour)
    {
        $paquetestours = self::query()->select('paquetes_tours.id', 'paquetes.nombre')
            ->join('paquetes', 'paquetes_tours.id_paquete', '=', 'paquetes.id')
            ->where('paquetes_tours.id_tour', '=', $id_tour)
            ->where('paquetes_tours.activo', '=', 1)
            ->orderBy('paquetes.nombre', 'Asc')
            ->get();

        return $paquetestours;
    }

    public static function getPaquetesTourByIdTour($id_tour)
    {
        $paquetestours = self::query()
            ->where('id_tour', '=', $id_tour)
            ->where('activo', '=', 1)
            ->orderBy('id', 'Asc')
            ->get();

        return $paquetestours;
    }

    public function tour()
    {
        return $this->belongsTo('App\Tours', 'id_tour');
    }

    public function paquete()
    {
        return $this->belongsTo('App\Paquetes', 'id_paquete');
    }

    public function reservacion()
    {
        return $this->hasMany('App\Reservaciones');
    }

    public function preciosTour() {
        return $this->hasMany(PrecioTour::class);
    }
}
