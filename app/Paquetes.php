<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    
	protected $table = 'paquetes';
    protected $fillable = ['id','nombre'];
    
    public static function getPaquetesTours($id_tour){
    	$paquetestours = self::Join('paquetes_tours', 'paquetes.id', '=', 'paquetes_tours.id_paquete')
    	->where('paquetes.activo',1)
    	->where('paquetes_tours.id_tour','=',$id_tour)
    	->orderBy('paquetes.nombre','Asc')
    	->get();

    	return $paquetestours;
    }

    public static function getNombrebyId($id_paquete){
    	$nombre = self::where('id','=',$id_paquete)->get();

    	return $nombre;  
   	}

   	public static function getPaquetesToursByIdPaqueteTour($id_paquete_tour){
        $paquetetour = self::Join('paquetes_tours', 'paquetes.id', '=', 'paquetes_tours.id_paquete')
            ->where('paquetes.activo',1)
            ->where('paquetes_tours.id','=',$id_paquete_tour)
            ->orderBy('paquetes.nombre','Asc')
            ->get();

        return $paquetetour;

    }

   	public function paqueteTour()
    {
        return $this->hasMany('App\PaquetesTours'); 
    }
}
