<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdiomasTours extends Model
{
    protected $table = 'idiomas_tours';
    protected $fillable = ['id','id_idioma','id_tour','activo'];


    public  function tour(){
    	return $this->belongsTo('App\Tours','id_tour');
    }

    public  function idioma(){
    	return $this->belongsTo('App\Idiomas','id_idioma');
    }

    public function reservacion(){
    	return $this->hasMany('App\Reservaciones');
    }
    public static function getIdiomasTourByIdTour($id_tour){
        $idomastours = self::where('activo',1)
        ->where('id_tour','=',$id_tour)
        ->orderBy('id','Asc')
        ->get();
        
        return $idomastours;
    }
}
