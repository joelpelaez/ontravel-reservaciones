<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreasHoteles extends Model
{
    //
    public static function getAreaHoteles($id_hotel){
    	$tours = self::where('activo',1)
    	->where('id_hotel','=',$id_hotel)
    	->orderBy('nombre','Asc')
    	->get();
    	return $tours;
    }
    
}
