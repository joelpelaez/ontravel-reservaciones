<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motivos extends Model
{
    public static function getMotivos(){
    	$motivos = self::where('activo',1)->orderBy('motivo','Asc')
    	->get();

    	return $motivos;
    }


    public function motivosStatus()
    {
        return $this->hasMany('App\MotivosStatus'); 
    }
}
