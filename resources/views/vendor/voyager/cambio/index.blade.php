@extends('voyager::master')

@section('page_title',' Resultados de Busqueda')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Tipos de cambio por agencia
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="panel panel-primary panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3>Búsqueda de tipos de cambio</h3>
                        </header>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="{{ route('tipocambio.mostrar.redirect') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="agencia_id">Agencia</label>
                                    <select id="agencia_id" name="agencia_id" class="form-control">
                                        @foreach($agencias as $agencia)
                                            <option value="{{ $agencia->id }}">{{ $agencia->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5 col-md-offset-2 form-group">
                                    <input type="submit" class="btn btn-primary" value="Mostrar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop