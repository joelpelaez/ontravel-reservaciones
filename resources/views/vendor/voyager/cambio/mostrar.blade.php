@extends('voyager::master')

@section('page_title',' Mostrar Tipos de Cambio')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .flex-container {
            display: flex;
            flex-wrap: wrap;
        }

        .child {
            width: 40%;
        }

        .end {
            margin-left: auto;
        }

        .end-col {
            margin-top: auto;
        }

        .flex-column {
            flex-direction: column;
        }

        .flex-row {
            flex-direction: row;
        }

        .bottom {
            align-self: flex-end;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Tipos de cambio por agencia
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3 style="padding-left: 20px">Actualizar de tipos de cambio</h3>
                        </header>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="post" action="{{ route('tipocambio.actualizar') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="agencia_id" value="{{ $agencia->id }}">
                            <div class="flex-container flex-row">
                                <div class="child">
                                    <label for="cambio">Tipo de Cambio Nuevo: </label>
                                    <input type="text" id="cambio" name="cambio" class="form-control">
                                </div>
                                <div class="child end flex-container flex-column">
                                    <input type="submit" class="btn btn-primary child end-col end" value="Actualizar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3 style="padding-left: 20px">Búsqueda de tipos de cambio</h3>
                        </header>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table">
                            <thead>
                            <tr>
                                <td>Tipo de Cambio</td>
                                <td>Periodo (Inicio)</td>
                                <td>Periodo (Fin)</td>
                                <td>Activo</td>
                                <td>Acciones</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tiposCambio as $tipoCambio)
                                <tr>
                                    <td>{{ $tipoCambio->cambio }}</td>
                                    <td>{{ $tipoCambio->fecha_inicio }}</td>
                                    <td>@if($tipoCambio->fecha_fin) {{ $tipoCambio->fecha_fin }} @else Hasta la
                                        fecha @endif </td>
                                    <td>@if($tipoCambio->activo) Sí @else No @endif</td>
                                    <td>
                                        <a href="{{ route('tipocambio.editar', ['id' => $tipoCambio->id]) }}" class="btn btn-xs btn-default">Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        let table = $('#dataTable').DataTable({
            "order": [[2, "desc"]],
            "paging": true,
            "dataSrc": "tableData",
            "oLanguage": {
                "sLoadingRecords": "Cargando...",
                "sSearch": "Buscar:",
                "sZeroRecords": "No se encontraron resultados",
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sEmptyTable": "No se encontraron resultados",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sUrl": "",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    </script>
@stop