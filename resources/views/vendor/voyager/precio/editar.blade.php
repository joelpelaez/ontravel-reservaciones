@extends('voyager::master')

@section('page_title','Editar Precio Tour')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .flex-container {
            display: flex;
            flex-wrap: wrap;
        }

        .child {
            width: 40%;
        }

        .end {
            margin-left: auto;
        }

        .end-col {
            margin-top: auto;
        }

        .flex-column {
            flex-direction: column;
        }

        .flex-row {
            flex-direction: row;
        }

        .bottom {
            align-self: flex-end;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Precio del Paquete Tour por Agencia
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3 style="padding-left: 20px">Actualizar de Precio de Tour</h3>
                        </header>
                    </div>
                    <div class="panel-body">

                        <form method="post" action="{{ route('precio.actualizarAnterior') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $id }}">
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="precio_adulto">Precio Adulto:</label>
                                    <input class="form-control" type="text" name="precio_adulto" id="precio_adulto" value="{{ $precio_adulto }}">
                                </div>
                                <div class="col-md-5 col-md-offset-2 form-group">
                                    <label for="precio_ninio">Precio Niñoo:</label>
                                    <input class="form-control" type="text" name="precio_ninio" id="precio_ninio" value="{{ $precio_ninio }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="fecha_inicio">Desde:</label>
                                    <input class="form-control" type="date" @if($fecha_inicio_min !== null) min="{{ $fecha_inicio_min }}" @endif @if($fecha_fin_max !== null) max="{{ $fecha_fin_max }}" @endif name="fecha_inicio" id="fecha_inicio" value="{{ $fecha_inicio }}">
                                </div>
                                <div class="col-md-5 col-md-offset-2 form-group">
                                    <label for="fecha_fin">Desde:</label>
                                    <input class="form-control" type="date" @if($fecha_inicio_min !== null) min="{{ $fecha_inicio_min }}" @endif @if($fecha_fin_max !== null) max="{{ $fecha_fin_max }}" @endif name="fecha_fin" id="fecha_fin" value="{{ $fecha_fin }}" @if(!$enable_fecha_fin) disabled @endif>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="flex-container flex-row">
                                        <div class="child end flex-container flex-column">
                                            <input type="submit" class="btn btn-primary child end-col end" value="Actualizar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
