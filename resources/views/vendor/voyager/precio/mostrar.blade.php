@extends('voyager::master')

@section('page_title',' Mostrar Precio de Paquete')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .flex-container {
            display: flex;
            flex-wrap: wrap;
        }

        .child {
            width: 25%;
            padding: 10px;
        }

        .end {
            margin-left: auto;
        }

        .end-col {
            margin-top: auto;
        }

        .flex-column {
            flex-direction: column;
        }

        .flex-row {
            flex-direction: row;
        }

        .bottom {
            align-self: flex-end;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Precio de los Paquetes de Tour por Agencia
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3 style="padding-left: 20px">Actualizar Precio del Paquete</h3>
                        </header>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="post" action="{{ route('precio.actualizar') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="agencia_id" value="{{ $agencia->id }}">
                            <input type="hidden" name="paquete_tour_id" value="{{ $paqueteTour->id }}">
                            <div class="flex-container flex-row">
                                <div class="child">
                                    <label for="precio_adulto">Precio Adulto Nuevo: </label>
                                    <input type="text" id="precio_adulto" name="precio_adulto" class="form-control">
                                </div>
                                <div class="child">
                                    <label for="precio_ninio">Precio Niño Nuevo: </label>
                                    <input type="text" id="precio_ninio" name="precio_ninio" class="form-control">
                                </div>
                                <div class="child">
                                    <label for="porcentaje_utilidad">Porcentaje Utilidad: </label>
                                    <input type="text" id="porcentaje_utilidad" name="porcentaje_utilidad" class="form-control">
                                </div>
                                <div class="child end flex-container flex-column">
                                    <input type="submit" class="btn btn-primary end-col end" value="Actualizar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3 style="padding-left: 20px">Lista de Precios - {{$paqueteTour->tour->nombre}} {{$paqueteTour->paquete->nombre}} para {{$agencia->nombre}}</h3>
                        </header>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table">
                            <thead>
                            <tr>
                                <td>Precio Adulto</td>
                                <td>Precio Niño</td>
                                <td>Porcentaje</td>
                                <td>Periodo (Inicio)</td>
                                <td>Periodo (Fin)</td>
                                <td>Activo</td>
                                <td>Acciones</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($precios as $precio)
                                <tr>
                                    <td>{{ $precio->precio_adulto }}</td>
                                    <td>{{ $precio->precio_ninio }}</td>
                                    <td>{{ $precio->porcentaje_utilidad }}</td>
                                    <td>{{ $precio->fecha_inicio }}</td>
                                    <td>@if($precio->fecha_fin) {{ $precio->fecha_fin }} @else Hasta la
                                        fecha @endif </td>
                                    <td>@if($precio->activo) Sí @else No @endif</td>
                                    <td>
                                        <a href="{{ route('precio.editar', ['id' => $precio->id]) }}" class="btn btn-xs btn-default">Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        let table = $('#dataTable').DataTable({
            "order": [[3, "desc"]],
            "paging": true,
            "dataSrc": "tableData",
            "oLanguage": {
                "sLoadingRecords": "Cargando...",
                "sSearch": "Buscar:",
                "sZeroRecords": "No se encontraron resultados",
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sEmptyTable": "No se encontraron resultados",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sUrl": "",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    </script>
@stop