@extends('voyager::master')

@section('page_title',' Resultados de Busqueda')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Tipos de cambio por agencia
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="panel panel-primary panel-bordered">
                    <div class="panel-heading">
                        <header>
                            <h3>Búsqueda de tipos de cambio</h3>
                        </header>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="{{ route('precio.mostrar.redirect') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="agencia_id">Agencia</label>
                                    <select id="agencia_id" name="agencia_id" class="form-control select2">
                                        <option value="" disabled="disabled" selected="selected">Seleccione una agencia...</option>
                                        @foreach($agencias as $agencia)
                                            <option value="{{ $agencia->id }}">{{ $agencia->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5 col-md-offset-2 form-group">
                                    <label for="tour_id">Tour</label>
                                    <select id="tour_id" name="tour_id" class="form-control select2" onchange="getPaquetesTours()">
                                        <option value="" disabled="disabled" selected="selected">Seleccione un tour...</option>
                                        @foreach($tours as $tour)
                                            <option value="{{ $tour->id }}">{{ $tour->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="paquete_tour_id">Paquete</label>
                                    <select id="paquete_tour_id" name="paquete_tour_id" class="form-control select2">
                                        <option value="" disabled="disabled" selected="selected">Seleccione un paquete...</option>
                                    </select>
                                </div>
                                <div class="col-md-5 col-md-offset-2 form-group">
                                    <input type="submit" class="btn btn-primary" value="Mostrar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        function getPaquetesTours() {
            let id = $("#tour_id option:selected").val();
            if (id !== "") {
                $.get('/getpaquetetours/' + id, function (data) {
                    $('#paquete_tour_id').empty();
                    $('#paquete_tour_id').append('<option value="" disabled="disabled" selected="selected">Seleccione un paquete...</option>');
                    $.each(data, function (index, pqtoursObj) {
                        $('#paquete_tour_id').append('<option value="' + pqtoursObj.id + '">' + pqtoursObj.nombre + '</option>');
                    })
                });
            }
        }
    </script>
@stop