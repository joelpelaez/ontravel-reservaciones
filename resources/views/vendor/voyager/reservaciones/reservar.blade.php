@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('page_title',' Nueva Reservacion')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Nueva Reservacion
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="alert alert-danger text-center" style="display:none;" id="error">
                    <strong>Advertencia: </strong>Debe completar todos los campos
                </div>
                <div class="alert alert-success text-center" style="display:none;" id="exito">
                    <strong>Felicidades: </strong>Su registro ha sido guardado
                </div>
                <div class="panel panel-bordered panel-success">
                    <!-- form start -->
                    <form method="POST" action="{{'store'}}" enctype="multipart/form-data" id="formReservacion">
                        <input type="hidden" id="id_region" name="id_region">
                        <div class="panel-body">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Información de la reserva</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-7">
                                    <button class="btn btn-default" id="add_rep_button" type="button"
                                            onclick="showAddRep();">Agregar un representante nuevo
                                    </button>
                                    <button class="btn btn-default" id="select_rep_button" type="button"
                                            onclick="showSelectRep();" style="display: none;">Seleccionar un
                                        representante
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label class="control-label" for="id_agencia">Agencia:</label>
                                    <select name="id_agencia" id="id_agencia"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('id_agencia');">
                                        <option disabled="disabled" selected="selected" value="">Seleccione una agencia...</option>
                                        @foreach($agencias as $agencia)
                                            <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                                        @endforeach
                                        <option value="-1">Sin Agencia</option>
                                    </select>
                                </div>
                                <div id="select_rep" class="form-group col-md-5 col-md-offset-2">
                                    <label class="control-label" for="id_rep">Represantante:</label>
                                    <select name="id_rep" id="id_rep"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('id_rep');">
                                        <option selected="selected" disabled="disabled" value="">
                                            Primero Seleccione una Agencia...
                                        </option>
                                        <option value="-1">Sin Representante</option>
                                    </select>
                                </div>
                                <div id="add_rep" class="form-group col-md-5 col-md-offset-2" style="display: none;">
                                    <label for="rep_name">Nombre del Representante:</label>
                                    <input type="text" id="rep_name" name="rep_name" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="id_tour">Tour: </label>
                                    <select name="id_tour" id="id_tour"
                                            class="form-control  select2 select2-hidden-accessible"
                                            onchange="validacion('id_tour');">
                                        <option value="">Seleccione un tour</option>
                                        @foreach($tours as $tour)
                                            <option value="{{$tour->id}}">{{$tour->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="id_paquete">Paquete:</label>
                                    <select name="id_paquete_tour" id="id_paquete"
                                            class="form-control  select2 select2-hidden-accessible"
                                            onchange="validacion('id_paquete');">
                                        <option value="">Primero seleccione un tour</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="id_hotel">Hotel:</label>
                                    <select name="id_hotel" id="id_hotel"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('id_hotel');">
                                        <option selected="selected" disabled="disabled" value="">
                                            Seleccione un hotel...
                                        </option>
                                        @foreach($hoteles as $hotel)
                                            <option value="{{$hotel->id}}">{{$hotel->nombre}}</option>
                                        @endforeach
                                        <option value="-1">Sin hotel</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="fecha_reserva">Fecha:</label>
                                    <input type="date" name="fecha_reserva" id="fecha_reserva" value="{{date('Y-m-d')}}"
                                           min="{{date('Y-m-d')}}" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div id="address-form" style="display: none;" class="form-group col-md-12">
                                    <label for="direccion">Dirección:</label>
                                    <input type="text" name="direccion" id="direccion" class="form-control">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Cliente & Pickup</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="apellido">Apellido:</label>
                                    <input name="apellido" id="apellido" class="form-control"
                                           type="text" onkeyup="validacion('apellido');"
                                           aria-describedby="apellidoHelpBlock">
                                    <span id="apellidoHelpBlock" class="help-block"></span>
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="habitacion">Habitacion:</label>
                                    <input type="text" name="habitacion" id="habitacion" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="id_areapickup">Lugar de Pickup:</label>
                                    <select name="id_areapickup" id="id_areapickup"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('id_areapickup');">
                                        <option value=""> Seleccione un Hotel</option>
                                    </select>
                                </div>

                                <div id="pickup-form" class="form-group col-md-5 col-md-offset-2">
                                    <label for="pickup">Pickup:</label>
                                    <select name="pickup" id="pickup"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('pickup');">
                                        <option value=""> Primero seleccione un lugar</option>
                                    </select>
                                </div>
                                <div id="pickup-custom" class="form-group col-md-5 col-md-offset-2"
                                     style="display: none">
                                    <label for="pickup_hora">Hora de Pickup:</label>
                                    <input type="text" class="form-control" name="pickup_hora" id="pickup_hora">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="id_idioma">Idioma:</label>
                                    <select name="id_idioma" id="id_idioma"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('id_idioma');">
                                        <option value="0">Primero seleccione un Tour</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="id_nacionalidad">Nacionalidad</label>
                                    <select name="id_nacionalidad" id="id_nacionalidad"
                                            class="form-control select2 select2-hidden-accessible"
                                            onchange="validacion('id_nacionalidad');">
                                        <option value="">Seleccione...</option>
                                        @foreach($nacionalidades as $nacionalidad)
                                            <option value="{{$nacionalidad->id}}">{{$nacionalidad->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div id="coupons">
                                <coupon-list-component
                                        tipos="{{ json_encode($tiposcupones) }}"></coupon-list-component>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Información adicional</h3>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="go_show">Go show</label>
                                    <input name="go_show" id="go_show" value="0" type="checkbox">
                                </div>
                                <div class="col-md-5">
                                    <label for="confirmacion">Clave de confirmación</label>
                                    <input type="text" class="form-control" id="confirmacion" name="confirmacion"
                                           placeholder="Autómatico">
                                </div>
                                <div class="form-group col-md-5">
                                    <input type="button" class="btn btn-success pull-right" value="Reservar"
                                           onclick="verificar();">
                                </div>
                            </div>
                        </div>

                    </form>

                    <!-- ===End of Form === -->

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ @url('/js/reservas.js') }}"></script>
    <script type="text/javascript">
        function showAddRep() {
            $('#select_rep').hide();
            $('#select_rep_button').show();
            $('#add_rep').show();
            $('#add_rep_button').hide();
            $('#id_rep').val(-1);
        }

        function showSelectRep() {
            $('#select_rep').show();
            $('#add_rep').hide();
            $('#select_rep_button').hide();
            $('#add_rep_button').show();
            $('#rep_name').val('');
        }

        function verificar() {

            let v1, v2, v3, v4, v5, v6, v8, v9, v10, v11;
            const v12 = 0;
            v1 = validacion('id_agencia');
            v2 = validacion('id_rep');
            v3 = validacion('id_tour');
            v4 = validacion('id_paquete');
            v5 = validacion('id_hotel');
            v6 = validarCupones();
            v8 = validacion('apellido');
            v9 = validacion('id_areapickup');
            v10 = validacion('pickup');
            v11 = validacion('id_idioma');


            if (v1 === false || v2 === false || v3 === false || v4 === false ||
                v5 === false || v6 === false || v8 === false || v9 === false ||
                v10 === false || v11 === false || v12 === false) {

                $("#exito").hide();
                $("#error").show();

            } else {
                $("#error").hide();
                $("#exito").show();
                $("#formReservacion").submit();
            }
        }

        function validacion(campo) {
            let indice = null;

            if (campo === 'id_agencia') {
                indice = document.getElementById(campo).selectedIndex;
                let el = $('#id_agencia');
                if (indice === null || indice === 0) {
                    el.parent().addClass("has-error");
                    el.parent().removeClass("has-success");
                    el.focus();
                } else {
                    el.parent().addClass("has-success");
                    el.parent().removeClass("has-error");
                }
            }

            if (campo === 'id_rep') {
                indice = document.getElementById(campo).selectedIndex;
                if ($('#rep_name').val() === "" && (indice == null || indice === 0 || indice === -1)) {
                    $('#id_rep').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-error");
                    $('#id_rep').focus();
                    return false;
                } else {
                    $('#id_rep').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-success");
                    return true;
                }
            }
            if (campo === 'id_tour') {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_tour').parent().attr("class", "form-group col-md-5 has-error");
                    $('#id_tour').focus();
                    return false;
                } else {
                    $('#id_tour').parent().attr("class", "form-group col-md-5 has-success");
                    return true;
                }
            }
            if (campo === 'id_paquete') {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_paquete').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-error");
                    $('#id_paquete').focus();
                    return false;
                } else {
                    $('#id_paquete').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-success");
                    return true;
                }
            }
            if (campo === 'id_hotel') {
                indice = document.getElementById(campo).selectedIndex;

                if ($('#id_hotel').val() === "-1") {
                    $('#address-form').show();
                    $('#pickup-form').hide();
                    $('#pickup-custom').show();
                    $('#id_areapickup').prop('disabled', true);
                } else {
                    $('#address-form').hide();
                    $('#pickup-form').show();
                    $('#pickup-custom').hide();
                    $('#id_areapickup').prop('disabled', false);
                }

                if (indice == null || indice === 0) {
                    $('#id_hotel').parent().attr("class", "form-group col-md-5 has-error");
                    $('#id_hotel').focus();
                    return false;
                } else {
                    $('#id_hotel').parent().attr("class", "form-group col-md-5 has-success");
                    return true;
                }
            }

            if (campo === 'apellido') {
                apellido = document.getElementById(campo).value;
                if (apellido == null || apellido.length === 0 || /^\s+$/.test(apellido)) {
                    $('#' + campo).parent().attr("class", "form-group col-md-5 has-error has-feedback");
                    $('#' + campo).parent().children('span').text("Ingrese el apellido").show();

                    return false;
                } else {
                    $('#' + campo).parent().attr("class", "form-group col-md-5 has-success has-feedback");
                    $('#' + campo).parent().children('span').hide();

                    return true;
                }
            }

            if (campo === 'id_areapickup' && $('#id_hotel').val() !== "-1") {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_areapickup').parent().attr("class", "form-group col-md-5 has-error");
                    $('#id_areapickup').focus();
                    return false;
                } else {
                    $('#id_areapickup').parent().attr("class", "form-group col-md-5 has-success");
                    return true;
                }
            }

            if (campo === 'pickup' && $('#id_hotel').val() !== "-1") {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#pickup').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-error");
                    $('#pickup').focus();
                    return false;
                } else {
                    $('#pickup').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-success");
                    return true;
                }
            }

            if (campo === 'id_idioma') {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_idioma').parent().attr("class", "form-group col-md-5  has-error");
                    $('#id_idioma').focus();
                    return false;
                } else {
                    $('#id_idioma').parent().attr("class", "form-group col-md-5  has-success");
                    return true;
                }
            }

            return true;
        }

        function validarCupones() {
            // Comprobar los numeros de adultos
            let result = true;

            $('.n_adultos_fields').each(function () {
                let el = $(this);
                if (el.val() === "0") {
                    el.parent().addClass("has-error");
                    el.parent().removeClass("has-success");
                    result = false;
                } else {
                    el.parent().removeClass("has-error");
                    el.parent().addClass("has-success");
                }
            });

            // Comprobar los cupones
            $('.cupon_fields').each(function () {
                let el = $(this);
                if (el.val() === "") {
                    el.parent().addClass("has-error");
                    el.parent().removeClass("has-success");
                    result = false;
                } else {
                    el.parent().removeClass("has-error");
                    el.parent().addClass("has-success");
                }
            });

            return result;
        }

        $("#go_show").click(function (event) {
            if ($(this).is(":checked")) $(this).val("1")
        });

        function getReps() {
            const id = $('#id_agencia option:selected').val();

            if (id === "-1") {
                $('#id_rep').empty();
                $('#id_rep').append('<option value="" disabled="disabled">Seleccione un representante... </option>');
                $('#id_rep').append('<option selected value="-1">Sin Representante</option>');
            } else {
                $.get('/getreps/' + id, function (data) {
                    $('#id_rep').empty();
                    $('#id_rep').append('<option value="" disabled="disabled" selected="selected">Seleccione un representante... </option>');
                    $.each(data, function (index, repsObj) {
                        $('#id_rep').append('<option value="' + repsObj.id + '">' + repsObj.nombre + '</option>');
                    });
                });
            }
        }

        function getTours() {
            const id = $("#id_hotel option:selected").attr('class');
            if (id !== "") {
                $.get('/gettours/' + id, function (data) {
                    $('#id_tour').empty();
                    $('#id_tour').append('<option value="" disabled="disabled" selected="selected">Seleccione un tour... </option>');
                    $.each(data, function (index, toursObj) {
                        $('#id_tour').append('<option value="' + toursObj.id_tour + '">' + toursObj.nombre + '</option>');
                    })
                });
            }
        }

        function getPaquetesTours() {

            let id = $("#id_tour option:selected").val();
            if (id !== "") {
                $.get('/getpaquetetours/' + id, function (data) {
                    $('#id_paquete').empty();
                    $('#id_paquete').append('<option value="" disabled="disabled" selected="selected">Seleccione un paquete...</option>');
                    $.each(data, function (index, pqtoursObj) {
                        $('#id_paquete').append('<option value="' + pqtoursObj.id + '">' + pqtoursObj.nombre + '</option>');
                    })
                });
            }
        }

        function getAreaHoteteles() {
            let id = $('#id_hotel option:selected').val();
            if (id !== "") {
                $.get('/getareahoteles/' + id, function (data) {
                    $('#id_areapickup').empty();
                    $('#id_areapickup').append('<option value="" disabled="disabled" selected="selected">Seleccione un lugar...</option>');
                    $.each(data, function (index, areaObj) {
                        $('#id_areapickup').append('<option value="' + areaObj.area_pickup + '">' + areaObj.area_pickup + '</option>');
                    })
                });
            }
        }

        function getPickupsHoteles() {
            const area = $('#id_areapickup option:selected').val();
            const id = $('#id_hotel option:selected').val();
            if (id !== "") {
                $.get('/getpickupshoteles/' + id + '/' + area, function (data) {
                    const select = $('#pickup');
                    select.empty();
                    select.append('<option value="" disabled="disabled" selected="selected"> Seleccione un pickup... </option>');
                    $.each(data, function (index, pickupObj) {
                        select.append('<option value="' + pickupObj.id + '">' + pickupObj.pickup + '</option>');
                    })
                });
            }
        }

        function getIdiomasTours() {
            const id = $('#id_tour option:selected').val();
            if (id !== "") {
                $.get('/getidiomastours/' + id, function (data) {
                    const select = $('#id_idioma');
                    select.empty();
                    select.append('<option value="" disabled="disabled" selected="selected"> Seleccione un idioma... </option>');
                    $.each(data, function (index, idiomaObj) {
                        select.append('<option value="' + idiomaObj.id + '">' + idiomaObj.nombre + '</option>');
                    })
                });
            }
        }

        function getHoteles() {
            const id_tour = $('#id_tour option:selected').val();
            if (id_tour !== "") {
                $.get('/getregiontour/' + id_tour, function (data) {
                    $.each(data, function (index, regionObj) {
                        const id_region = regionObj.id;
                        $.get('/gethoteles/' + id_region, function (data) {
                            $.each(data, function (index, hotelObj) {
                                const region = hotelObj.id;
                            })
                        });
                    })
                });
            }
        }


        $("#id_agencia").change(function () {
            getReps();
        });
        $("#id_hotel").change(function () {
            getAreaHoteteles();
        });
        $("#id_tour").change(function () {
            getPaquetesTours();
            getIdiomasTours();
        });
        $("#id_areapickup").change(function () {
            getPickupsHoteles();
        });

    </script>
@stop
