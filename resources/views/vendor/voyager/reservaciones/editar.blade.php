@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .btn.btn-info {
            border: 0 !important;
        }
    </style>
@stop
@section('page_title',' Editar Reservación')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Editar Reservacion
    </h1>
    <a class="btn btn-sm btn-success" href="{{ route('reservaciones.efectivo', ['id' => $reservacion->id]) }}">
        Efectivo
    </a>
    <a class="btn btn-sm btn-info" href="{{ route('reservaciones.goshow', ['id' => $reservacion->id]) }}">
        Go Show
    </a>
    <a class="btn btn-sm btn-warning" href="{{ route('reservaciones.noshow', ['id' => $reservacion->id]) }}">
        No Show
    </a>
    |
    <button type="button" class="btn btn-sm btn-warning " data-id="{{$reservacion->id}}"
            title="Reagendar" data-toggle="modal" data-target="#reagendar">
        <i class="voyager-calendar"></i> <span class="hidden-xs hidden-sm">Reagendar</span>
    </button>
    <button type="button" class="btn btn-sm btn-danger " data-id="{{$reservacion->id}}"
            title="Cancelar" data-toggle="modal" data-target="#myModal">
        <i class="voyager-skull"></i> <span class="hidden-xs hidden-sm">Cancelar</span>
    </button>

    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group col-md-5">
                    <h1 class="panel-title alert alert-success text-center span2">
                        Confirmación: {{$reservacion->confirmacion}}</h1>
                </div>
                @if($reservacion->statusReservacion->id == 1 || $reservacion->statusReservacion->id == 4 ||
                $reservacion->statusReservacion->id == 6)
                    <div class="col-md-5 col-md-offset-2">
                        <h1 class="panel-title alert alert-info text-center span2">Status de
                            reservación: {{$reservacion->statusReservacion->nombre}}
                        </h1>
                    </div>
                @elseif($reservacion->statusReservacion->id == 2 || $reservacion->statusReservacion->id == 5)
                    <div class="col-md-5 col-md-offset-2">
                        <h1 class="panel-title alert alert-warning text-center span2">Status de
                            reservación: {{$reservacion->statusReservacion->nombre}}
                        </h1>
                    </div>
                @else
                    <div class="col-md-5 col-md-offset-2">
                        <h1 class="panel-title alert alert-danger text-center span2">Status de
                            reservación: {{$reservacion->statusReservacion->nombre}}
                        </h1>
                    </div>
                @endif
            </div>

        </div>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="alert alert-danger text-center" style="display:none;" id="error">
                    <strong>Advertencia: </strong>Debe completar todos los campos
                </div>
                <div class="alert alert-success text-center" style="display:none;" id="exito">
                    <strong>Felicidades: </strong>Su registro ha sido guardado
                </div>
                <div class="panel panel-bordered panel-success">
                    <!-- form start -->
                    <form method="POST" action="{{'update'}}" enctype="multipart/form-data" id="formReserva">
                        <div class="panel-body">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="id" value="{{$reservacion->id}}">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Información de la reserva</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="id_agencia">Agencia:</label>
                                    <select name="id_agencia" id="id_agencia"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="" disabled>Seleccione una agencia</option>
                                        @foreach($agencias as $agencia)
                                            <option @if($reservacion->id_agencia==$agencia->id) selected
                                                    @endif value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                                        @endforeach
                                        <option @if ($reservacion->id_agencia == -1) selected @endif
                                        value="-1">Sin agencia
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="id_rep">Representante:</label>
                                    <select name="id_rep" id="id_rep"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="" disabled>Seleccione un representante</option>
                                        @foreach($representantes as $rep)
                                            <option @if($reservacion->id_representante == $rep->id) selected
                                                    @endif value="{{$rep->id}}">{{$rep->nombre}}</option>
                                        @endforeach
                                        <option @if($reservacion->id_representante === -1) selected
                                                @endif value="-1">Sin Representante
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5 ">
                                    <label for="id_tour">Tour: </label>
                                    <select name="id_tour" id="id_tour"
                                            class="form-control  select2 select2-hidden-accessible">
                                        <option value="">Seleccione un tour</option>
                                        @foreach($tours as $tour)
                                            <option @if($reservacion->paqueteTour->tour->id == $tour->id) selected
                                                    @endif value="{{$tour->id}}">{{$tour->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="id_paquete">Paquete:</label>
                                    <select name="id_paquete_tour" id="id_paquete"
                                            class="form-control  select2 select2-hidden-accessible">
                                        <option value="">Seleccione un paquete</option>
                                        @foreach($paquetestours as $paquete)
                                            <option @if($reservacion->paqueteTour->paquete->id == $paquete->paquete->id) selected
                                                    @endif value="{{$paquete->id}}">{{$paquete->paquete->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5 ">
                                    <label for="id_hotel">Hotel:</label>
                                    <select name="id_hotel" id="id_hotel" onchange="validarHotel()"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Seleccione un hotel</option>
                                        @foreach($hoteles as $hotel)
                                            <option @if($reservacion->id_hotel==$hotel->id) selected
                                                    @endif class="{{$hotel->id_region}}"
                                                    value="{{$hotel->id}}">{{$hotel->nombre}}</option>
                                        @endforeach
                                        <option @if($reservacion->id_hotel==-1) selected
                                                @endif value="-1">Sin Hotel
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="fecha_reserva">Fecha:</label>
                                    <input type="date" name="fecha_reserva" id="fecha_reserva"
                                           value="{{ $fecha_reservacion }}" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div id="address-form" @if ($reservacion->id_hotel !== -1) style="display: none;" @endif
                                class="form-group col-md-12">
                                    <label for="direccion">Dirección:</label>
                                    <input type="text" name="direccion" id="direccion"
                                           value="{{ $reservacion->direccion }}" class="form-control">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Cliente & Pickup</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="apellido">Apellido:</label>
                                    <input name="apellido" required="required" id="apellido"
                                           value="{{$reservacion->apellido}}" class="form-control required" type="text">

                                </div>
                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="habitacion">Habitación:</label>
                                    <input name="habitacion" id="habitacion"
                                           value="{{$reservacion->habitacion}}" class="form-control" type="text">
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="id_areapickup">Lugar de Pickup:</label>
                                    <select name="id_areapickup" id="id_areapickup"
                                            class="form-control select2 select2-hidden-accessible"
                                            @if ($reservacion->id_hotel == -1) disabled="disabled" @endif >
                                        <option value=""> Seleccione un lugar de pickup</option>
                                        @foreach($pickupshoteles as $pickuphotel)
                                            <option @if($reservacion->pickup !== null && $reservacion->pickup->area_pickup == $pickuphotel->area_pickup) selected
                                                    @endif value="{{$pickuphotel->area_pickup}}">{{$pickuphotel->area_pickup}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="pickup-form" class="form-group col-md-5 col-md-offset-2"
                                     @if ($reservacion->id_hotel === -1) style="display: none" @endif>
                                    <label for="pickup">Pickup:</label>
                                    <select name="id_pickup_hotel" id="pickup"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Seleccione un pickup</option>
                                        @foreach($pickupsarea as $pickuparea)
                                            <option @if($reservacion->pickup !== null && $reservacion->pickup->id == $pickuparea->id) selected
                                                    @endif value="{{$pickuparea->id}}">{{$pickuparea->pickup}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="pickup-custom" class="form-group col-md-5 col-md-offset-2"
                                     @if ($reservacion->id_hotel !== -1) style="display: none" @endif>
                                    <label for="pickup_hora">Hora de Pickup:</label>
                                    <input type="text" class="form-control" name="pickup_hora" id="pickup_hora"
                                           value="{{ date('H:i', strtotime($reservacion->pickup_hora)) }}">
                                </div>

                                <div class="form-group col-md-5">
                                    <label for="id_idioma">Idioma:</label>
                                    <select name="id_idioma" id="id_idioma"
                                            class="form-control select2 select2-hidden-accessible" required="required">

                                        <option value="0">seleccione un Idioma</option>
                                        @foreach($idiomasTour as $idioma)
                                            <option @if($reservacion->idiomaTour->idioma->nombre ==$idioma->idioma->nombre) selected
                                                    @endif value="{{$idioma->id}}">{{$idioma->idioma->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="id_nacionalidad">Nacionalidad</label>
                                    <select name="id_nacionalidad" id="id_nacionalidad"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Seleccione...</option>
                                        @foreach($nacionalidades as $nacionalidad)
                                            <option @if($reservacion->id_nacionalidad==$nacionalidad->id) selected
                                                    @endif value="{{$nacionalidad->id}}">{{$nacionalidad->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="coupons">
                                        <coupon-list-component tipos='{{ json_encode($tiposcupones) }}'
                                                               cupones='{{ json_encode($cupones) }}'
                                                               v-bind:edit-mode="true"></coupon-list-component>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <button type="button" id="btn_guardar" name="btn_guardar"
                                            class="btn btn-success pull-right" onclick="verificar()">
                                        Actualizar Información
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- ===End of Form === -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal cancelar-->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Cancelar Reservación</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p>Elija el motivo de la cancelación</p>
                    <form method="POST" action="" enctype="multipart/form-data" id="formCancelacion">
                        {{ csrf_field() }}
                        @foreach($motivos as $motivo)
                            <input type="radio" name="motivos" value="{{$motivo->id}}">
                            <label for="{{$motivo->motivo}}">{{$motivo->motivo}}</label>
                            <br>
                        @endforeach
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <input id="btnEnviar" type="submit" class="btn btn-sm btn-success " value="Aceptar">
                </div>
            </div>
        </div>
    </div>

    <!-- Modal reagendar-->
    <div class="modal fade" id="reagendar">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Reagendar Reservación</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p>Elija nueva fecha y el motivo de la reagenda</p>
                    <form method="POST" action="" enctype="multipart/form-data" id="formReagenda">
                        {{ csrf_field() }}
                        <div class="form-group col-md-5 ">
                            <label for="Fecha">Fecha:</label>
                            <input type="date" name="fecha_reservacion" id="fecha_reserva" value="{{date('Y-m-d')}}"
                                   min="{{date('Y-m-d')}}" class="form-control">
                        </div>
                        @foreach($motivos as $motivo)
                            <input type="radio" name="motivos" value="{{$motivo->id}}">
                            <label for="{{$motivo->motivo}}">{{$motivo->motivo}}</label>
                            <br>
                        @endforeach
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <input id="btn" type="submit" class="btn btn-sm btn-success " value="Aceptar">
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ @url('/js/reservas.js') }}"></script>
    <script>
        function showAddRep() {
            $('#select_rep').hide();
            $('#select_rep_button').show();
            $('#add_rep').show();
            $('#add_rep_button').hide();
            $('#id_rep').val(-1);
        }

        function showSelectRep() {
            $('#select_rep').show();
            $('#add_rep').hide();
            $('#select_rep_button').hide();
            $('#add_rep_button').show();
            $('#rep_name').val('');
        }

        function verificar() {

            let v1, v2, v3, v4, v5, v6, v8, v9, v10, v11;
            const v12 = 0;
            v1 = validacion('id_agencia');
            v2 = validacion('id_rep');
            v3 = validacion('id_tour');
            v4 = validacion('id_paquete');
            v5 = validacion('id_hotel');
            v8 = validacion('apellido');
            v9 = validacion('id_areapickup');
            v10 = validacion('pickup');
            v11 = validacion('id_idioma');
            v6 = validarCupones();


            if (v1 === false || v2 === false || v3 === false || v4 === false ||
                v5 === false || v6 === false || v8 === false || v9 === false ||
                v10 === false || v11 === false || v12 === false) {

                $("#exito").hide();
                $("#error").show();

            } else {
                $("#error").hide();
                $("#exito").show();
                $("#formReserva").submit();
            }
        }

        function validacion(campo) {
            let indice = null;

            if (campo === 'id_agencia') {
                indice = document.getElementById(campo).selectedIndex;
                let el = $('#id_agencia');
                if (indice === null || indice === 0) {
                    el.parent().addClass("has-error");
                    el.parent().removeClass("has-success");
                    el.focus();
                } else {
                    el.parent().addClass("has-success");
                    el.parent().removeClass("has-error");
                }
            }

            if (campo === 'id_rep') {
                indice = document.getElementById(campo).selectedIndex;
                if ($('#rep_name').val() === "" && (indice == null || indice === 0 || indice === -1)) {
                    $('#id_rep').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-error");
                    $('#id_rep').focus();
                    return false;
                } else {
                    $('#id_rep').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-success");
                    return true;
                }
            }
            if (campo === 'id_tour') {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_tour').parent().attr("class", "form-group col-md-5 has-error");
                    $('#id_tour').focus();
                    return false;
                } else {
                    $('#id_tour').parent().attr("class", "form-group col-md-5 has-success");
                    return true;
                }
            }
            if (campo === 'id_paquete') {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_paquete').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-error");
                    $('#id_paquete').focus();
                    return false;
                } else {
                    $('#id_paquete').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-success");
                    return true;
                }
            }
            if (campo === 'id_hotel') {
                indice = document.getElementById(campo).selectedIndex;

                if ($('#id_hotel').val() === "-1") {
                    $('#address-form').show();
                    $('#pickup-form').hide();
                    $('#pickup-custom').show();
                    $('#id_areapickup').prop('disabled', true);
                } else {
                    $('#address-form').hide();
                    $('#pickup-form').show();
                    $('#pickup-custom').hide();
                    $('#id_areapickup').prop('disabled', false);
                }

                if (indice == null || indice === 0) {
                    $('#id_hotel').parent().attr("class", "form-group col-md-5 has-error");
                    $('#id_hotel').focus();
                    return false;
                } else {
                    $('#id_hotel').parent().attr("class", "form-group col-md-5 has-success");
                    return true;
                }
            }

            if (campo === 'apellido') {
                apellido = document.getElementById(campo).value;
                if (apellido == null || apellido.length === 0 || /^\s+$/.test(apellido)) {
                    $('#' + campo).parent().attr("class", "form-group col-md-5 has-error has-feedback");
                    $('#' + campo).parent().children('span').text("Ingrese el apellido").show();
                    $('#' + campo).focus();

                    return false;
                } else {
                    $('#' + campo).parent().attr("class", "form-group col-md-5 has-success has-feedback");
                    $('#' + campo).parent().children('span').hide();

                    return true;
                }
            }

            if (campo === 'id_areapickup' && $('#id_hotel').val() !== "-1") {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_areapickup').parent().attr("class", "form-group col-md-5 has-error");
                    $('#id_areapickup').focus();
                    return false;
                } else {
                    $('#id_areapickup').parent().attr("class", "form-group col-md-5 has-success");
                    return true;
                }
            }

            if (campo === 'pickup' && $('#id_hotel').val() !== "-1") {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#pickup').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-error");
                    $('#pickup').focus();
                    return false;
                } else {
                    $('#pickup').parent().attr("class", "form-group col-md-5 col-md-offset-2 has-success");
                    return true;
                }
            }

            if (campo === 'id_idioma') {
                indice = document.getElementById(campo).selectedIndex;
                if (indice == null || indice === 0) {
                    $('#id_idioma').parent().attr("class", "form-group col-md-5  has-error");
                    $('#id_idioma').focus();
                    return false;
                } else {
                    $('#id_idioma').parent().attr("class", "form-group col-md-5  has-success");
                    return true;
                }
            }

            return true;
        }

        function validarCupones() {
            // Comprobar los numeros de adultos
            let result = true;

            $('.n_adultos_fields').each(function () {
                let el = $(this);
                if (el.val() === "0") {
                    el.parent().addClass("has-error");
                    el.parent().removeClass("has-success");
                    el.focus();
                    result = false;
                } else {
                    el.parent().removeClass("has-error");
                    el.parent().addClass("has-success");
                }
            });

            // Comprobar los cupones
            $('.cupon_fields').each(function () {
                let el = $(this);
                if (el.val() === "") {
                    el.parent().addClass("has-error");
                    el.parent().removeClass("has-success");
                    el.focus();
                    result = false;
                } else {
                    el.parent().removeClass("has-error");
                    el.parent().addClass("has-success");
                }
            });

            return result;
        }

        function validarHotel() {
            indice = document.getElementById('id_hotel').selectedIndex;

            if ($('#id_hotel').val() === "-1") {
                $('#address-form').show();
                $('#pickup-form').hide();
                $('#pickup-custom').show();
                $('#id_areapickup').prop('disabled', true);
            } else {
                $('#address-form').hide();
                $('#pickup-form').show();
                $('#pickup-custom').hide();
                $('#id_areapickup').prop('disabled', false);
            }

            if (indice == null || indice === 0) {
                $('#id_hotel').parent().attr("class", "form-group col-md-5 has-error");
                $('#id_hotel').focus();
                return false;
            } else {
                $('#id_hotel').parent().attr("class", "form-group col-md-5 has-success");
                return true;
            }
        }

        $('#myModal').on('show.bs.modal', function (e) {
            const id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formCancelacion"]').attr('action', "{{'cancelar'}}");

            $(e.currentTarget).find('input[id="btnEnviar"]').click(function (event) {
                $('#formCancelacion').submit();
            });
        });

        $('#reagendar').on('show.bs.modal', function (e) {
            const id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formReagenda"]').attr('action', "{{'reagendar'}}");

            $(e.currentTarget).find('input[id="btn"]').click(function (event) {
                $('#formReagenda').submit();
            });

        });

        $("#go_show").click(function (event) {
            if ($(this).is(":checked")) $(this).val("1")
        });
        $("#no_show").click(function (event) {
            if ($(this).is(":checked")) $(this).val("1")
        });

        function getReps() {
            const id = $('#id_agencia option:selected').val();
            if (id === "-1") {
                $('#id_rep').empty();
                $('#id_rep').append('<option selected disabled value="-1">Sin Representante</option>');
            } else {
                $.get('/getreps/' + id, function (data) {
                    $('#id_rep').empty();
                    $('#id_rep').append('<option value="" disabled="disabled" selected="selected">Seleccione un representante... </option>');
                    $.each(data, function (index, repsObj) {
                        $('#id_rep').append('<option value="' + repsObj.id + '">' + repsObj.nombre + '</option>');
                    });
                });
            }
        }

        function getTours() {
            //var id=$('#id_hotel').val();
            const id = $("#id_hotel option:selected").attr('class');
            $.get('/gettours/' + id, function (data) {
                $('#id_tour').empty();
                $('#id_tour').append('<option value="" disabled="disabled" selected="selected"> Seleccione un tour... </option>');
                $.each(data, function (index, toursObj) {
                    $('#id_tour').append('<option value="' + toursObj.id_tour + '">' + toursObj.nombre + '</option>');
                })
            });
        }

        function getPaquetesTours() {
            const id = $("#id_tour option:selected").val();
            if (id !== "") {
                $.get('/getpaquetetours/' + id, function (data) {
                    $('#id_paquete').empty();
                    $('#id_paquete').append('<option value="" disabled="disabled" selected="selected"> Seleccione un paquete... </option>');
                    $.each(data, function (index, pqtoursObj) {
                        $('#id_paquete').append('<option value="' + pqtoursObj.id + '">' + pqtoursObj.nombre + '</option>');
                    })
                });
            }
        }

        function getAreaHoteteles() {
            const id = $('#id_hotel option:selected').val();
            $.get('/getareahoteles/' + id, function (data) {
                $('#id_areapickup').empty();
                $('#id_areapickup').append('<option value="" disabled="disabled" selected="selected"> Seleccione un lugar... </option>');
                $.each(data, function (index, areaObj) {
                    $('#id_areapickup').append('<option value="' + areaObj.area_pickup + '">' + areaObj.area_pickup + '</option>');
                })
            });
        }

        function getPickupsHoteles() {
            const area = $('#id_areapickup option:selected').val();
            const id = $('#id_hotel option:selected').val();
            debugger;
            $.get('/getpickupshoteles/' + id + '/' + area, function (data) {
                $('#pickup').empty();
                $('#pickup').append('<option value="" disabled="disabled" selected="selected"> Seleccione un pickup... </option>');

                $.each(data, function (index, pickupObj) {
                    $('#pickup').append('<option value="' + pickupObj.id + '">' + pickupObj.pickup + '</option>');
                })
            });
        }

        function getIdiomasTours() {
            const id = $('#id_tour option:selected').val();
            $.get('/getidiomastours/' + id, function (data) {
                $('#id_idioma').empty();
                $('#id_idioma').append('<option value="" disabled="disabled" selected="selected"> Seleccione un idioma... </option>');
                $.each(data, function (index, idiomaObj) {
                    $('#id_idioma').append('<option value="' + idiomaObj.id + '">' + idiomaObj.nombre + '</option>');
                })
            });
        }

        $("#id_agencia").change(function () {
            getReps();
        });

        $("#id_hotel").change(function () {
            getAreaHoteteles();
        });

        $("#id_tour").change(function () {
            getPaquetesTours();
            getIdiomasTours();
        });
        $("#id_areapickup").change(function () {
            getPickupsHoteles();
        });
    </script>
@stop
