@extends('voyager::master')

@section('page_title',' Resultados de Busqueda')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #777777
        }

        .btn-info {
            border-width: 0 !important;
        }
    </style>

@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Resultados de búsqueda
    </h1>
    <a type="button" class="btn btn-sm btn-dark" title="Regresar" href="buscar">
        <i class="voyager-double-left"></i> <span class="hidden-xs hidden-sm">Regresar</span>
    </a>
    <form style="display: inline;" method="post" target="_blank" enctype="multipart/form-data"
          action="resultados/excel">
        {{ csrf_field() }}
        <input type="hidden" name="id_hotel" value="{{ $request->id_model }}">
        <input type="hidden" name="id_agencia" value="{{ $request->id_agencia }}">
        <input type="hidden" name="id_representante" value="{{ $request->id_representante }}">
        <input type="hidden" name="id_tour" value="{{ $request->id_tour }}">
        <input type="hidden" name="id_paquete_tour" value="{{ $request->id_paquete_tour }}">
        <input type="hidden" name="apellido" value="{{ $request->apellido }}">
        <input type="hidden" name="fecha_reservacion" value="{{ $request->fecha_reservacion }}">
        <input type="hidden" name="fecha_inicio" value="{{ $request->fecha_inicio }}">
        <input type="hidden" name="fecha_fin" value="{{ $request->fecha_fin }}">
        <input type="submit" value="Exportar a Excel" class="btn btn-sm btn-primary">
    </form>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Confirmación</th>
                                <th>Tour</th>
                                <th>Paquete</th>
                                <th>Apellido</th>
                                <th>Agencia</th>
                                <th>Representante</th>
                                <th>Fecha</th>
                                <th>Status</th>
                                <th>Creado por</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($reservas) > 0)
                                @foreach($reservas as $reserva)
                                    <tr>
                                        <td>
                                            {{$reserva->confirmacion}}
                                        </td>
                                        <td>
                                            {{$reserva->paqueteTour->tour->nombre}}
                                        </td>
                                        <td>
                                            {{$reserva->paqueteTour->paquete->nombre}}
                                        </td>
                                        <td>
                                            {{$reserva->apellido}}
                                        </td>
                                        <td>
                                            @if ($reserva->agencia)
                                                {{$reserva->agencia->nombre}}
                                            @else
                                                Sin Agencia
                                            @endif
                                        </td>
                                        <td>
                                            @if ($reserva->representante)
                                                {{$reserva->representante->nombre}}
                                            @else
                                                Sin representante
                                            @endif
                                        </td>
                                        <td>
                                            {{ $reserva->fecha_reservacion->format('Y/m/d') }}
                                        </td>
                                        <td id="status{{$reserva->id}}">
                                            {{ $reserva->statusReservacion->nombre }}
                                        </td>
                                        <td>
                                                {{ $reserva->user->name }}
                                        </td>
                                        @if($reserva->id_status_reservacion == 3)
                                            <td>
                                                <a href="{{ route('reservaciones.show', $reserva->id) }}"
                                                   title="ver" class="btn btn-sm btn-success">
                                                    <i class="voyager-eye"></i>
                                                </a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="{{ route('reservaciones.show', $reserva->id) }}"
                                                   title="ver" class="btn btn-sm btn-success">
                                                    <i class="voyager-eye"></i>
                                                </a>
                                                <a href="{{$reserva->id}}/{{'editar'}}" title="editar"
                                                   class="btn btn-sm btn-warning ">
                                                    <i class="voyager-edit"></i>
                                                </a>
                                                <button type="button" class="btn btn-sm btn-danger "
                                                        data-id="{{$reserva->id}}"
                                                        title="Cancelar" data-toggle="modal" data-target="#myModal">
                                                    <i class="voyager-skull"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-info state"
                                                        data-id="{{ $reserva->id }}" data-action="efectivo"
                                                        title="Efectivo">
                                                    <i class="voyager-check"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-success state"
                                                        data-id="{{ $reserva->id }}" data-action="goshow"
                                                        title="Go Show">
                                                    <i class="voyager-star"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-warning state"
                                                        data-id="{{ $reserva->id }}" data-action="noshow"
                                                        title="No Show">
                                                    <i class="voyager-alarm-clock"></i>
                                                </button>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Cancelar Reservación</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    @if (count($reservas) > 0)
                        <form method="POST" action="" enctype="multipart/form-data" id="formReservacion">
                            {{ csrf_field() }}
                            @foreach($motivos as $motivo)
                                <input type="radio" name="motivos" value="{{$motivo->id}}"> <label
                                        for="{{$motivo->motivo}}">{{$motivo->motivo}}</label><br>
                            @endforeach
                        </form>

                    @endif
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <input id="btnEnviar" type="submit" class="btn btn-sm btn-success " value="Aceptar">
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "paging": true,
                "dataSrc": "tableData",
                "oLanguage": {
                    "sLoadingRecords": "Cargando...",
                    "sSearch": "Buscar:",
                    "sZeroRecords": "No se encontraron resultados",
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sEmptyTable": "No se encontraron resultados",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

        $('#myModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formReservacion"]').attr('action', id + "/{{'cancelar'}}");

            $(e.currentTarget).find('input[id="btnEnviar"]').click(function (event) {
                $('#formReservacion').submit();
            });
        });

        $('.state').on('click', function (e) {
            const id = $(e.currentTarget).data('id');
            const action = $(e.currentTarget).data('action');
            const url = '/reservaciones/' + id + '/' + action + '?fast';

            $.get(url, function (data) {
                $('#status' + id).text(data);
            })
        })

    </script>
@stop
