@extends('voyager::master')

@section('page_title',' Ver Reservación')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #777777
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-telephone"></i> Ver Reservación
    </h1>
    <a href="{{'reservar'}}" class="btn btn-default  span2"><i class="icon voyager-list-add"></i>
        <span class="hidden-xs hidden-sm">Realizar otra reservacion</span></a>

    @if($reservacion->statusReservacion->id!=3)
        <a href="{{$reservacion->id}}/{{'editar'}}" class="btn btn-primary  span2"><i class="icon voyager-edit"></i>
            <span class="hidden-xs hidden-sm">Editar</span></a>
    @endif

    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-5 col-md-offset-4">
                <h1 class="panel-title alert alert-success text-center span4">
                    Confirmación: {{$reservacion->confirmacion}}
                </h1>
            </div>
            @if($reservacion->statusReservacion->id == 1 || $reservacion->statusReservacion->id == 4 ||
                $reservacion->statusReservacion->id == 6)
                <div class="col-md-5 col-md-offset-4">
                    <h1 class="panel-title alert alert-info text-center span4">Status de
                        reservación: {{$reservacion->statusReservacion->nombre}}
                    </h1>
                </div>
            @elseif($reservacion->statusReservacion->id == 2 || $reservacion->statusReservacion->id == 5)
                <div class="col-md-5 col-md-offset-4">
                    <h1 class="panel-title alert alert-warning text-center span4">Status de
                        reservación: {{$reservacion->statusReservacion->nombre}}
                    </h1>
                </div>
            @else
                <div class="col-md-5 col-md-offset-4">
                    <h1 class="panel-title alert alert-danger text-center span4">Status de
                        reservación: {{$reservacion->statusReservacion->nombre}}
                    </h1>
                </div>
            @endif
            <div align="center" class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <h1>
                            {{$reservacion->paqueteTour->tour->nombre}}
                            {{$reservacion->paqueteTour->paquete->nombre}}, para el
                            {{$reservacion->fecha_reservacion->format('l j \d\e F \d\e\l Y')}}
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Información del Pax</h3>
                            </div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-md-6">
                                        <h3>Apellido:</h3>
                                        <h4>{{$reservacion->apellido }}</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <h3>Habitación:</h3>
                                        <h4>{{$reservacion->habitacion }}</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="container">
                                    @if ($reservacion->pickup !== null)
                                        <div class="col-md-6">
                                            <h3>Lugar de Pickup:</h3>
                                            <h4>{{$reservacion->pickup->area_pickup }}</h4>
                                        </div>

                                        <div class="col-md-6">
                                            <h3>Horario del Pickup:</h3>
                                            <h4>{{$reservacion->pickup->pickup }}</h4>
                                        </div>
                                    @else
                                        <div class="col-md-6">
                                            <h3>Lugar de Pickup:</h3>
                                            <h4>{{$reservacion->direccion }}</h4>
                                        </div>

                                        <div class="col-md-6">
                                            <h3>Horario del Pickup:</h3>
                                            <h4>{{$reservacion->pickup_hora }}</h4>
                                        </div>
                                    @endif
                                </div>
                                <hr>
                                <div class="container">
                                    <div class="col-md-6">
                                        <h3>Idioma:</h3>
                                        <h4>{{$reservacion->idiomaTour->idioma->nombre }}</h4>
                                    </div>

                                    <div class="col-md-6">
                                        <h3>Nacionalidad:</h3>
                                        @if($reservacion->id_nacionalidad!=0)
                                            <h4>{{$reservacion->nacionalidad->nombre }}</h4>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Información de la agencia</h3>
                            </div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-md-6">
                                        @if ($reservacion->agencia !== null)
                                            <h3>Agencia:</h3>
                                            <h4>{{$reservacion->agencia->nombre }}</h4>
                                        @else
                                            <h3>Agencia:</h3>
                                            <h4>Reservación personal</h4>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        @if ($reservacion->representante !== null)
                                            <h3>Rep:</h3>
                                            <h4>{{$reservacion->representante->nombre }}</h4>
                                        @else
                                            <h3>Representante:</h3>
                                            <h4>N/A</h4>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Hotel de operación:</h3>
                                        @if ($reservacion->hotel !== null)
                                            <h4>{{$reservacion->hotel->nombre }}</h4>
                                        @else
                                            <h4>Sin hotel</h4>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3>Fecha de Venta:</h3>
                                        <h4>{{ (new Datetime($reservacion->fecha_venta))->format('d/m/y') }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Cupones</h3>
                            </div>
                            <div class="panel-body">
                                @foreach($cupones as $key => $cupon)
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3>Cupon #{{$key + 1}}: {{ $cupon->cupon }}</h3>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <h3>Tipo:</h3>
                                            <h4>{{ $cupon->tipoCupon->nombre }}</h4>
                                        </div>
                                        <div class="col-md-3">
                                            <h3>Adultos:</h3>
                                            <h4>{{ $cupon->n_adultos }}</h4>
                                        </div>
                                        <div class="col-md-3">
                                            <h3>Niños:</h3>
                                            <h4>{{ $cupon->n_ninos }}</h4>
                                        </div>
                                        <div class="col-md-3">
                                            <h3>Infantes:</h3>
                                            <h4>{{ $cupon->n_infantes }}</h4>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3>Confirmación cupón: </h3>
                                            <h4>{{ $cupon->confirmacion }}</h4>
                                        </div>
                                        <div class="col-md-6">
                                            <h3>Observaciones: </h3>
                                            <h4>{{ $cupon->observaciones }}</h4>
                                        </div>
                                    </div>
                                    @if ($key + 1 !== count($cupones))
                                        <hr style="border-top: 2px solid #58bc1a">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-body voyager">
                        <h2>Histórico de cambios</h2>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Fecha de Modificación</th>
                                <th>Estado Actual</th>
                                <th>Estado Anterior</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($historico as $registro)
                                <tr>
                                    <td>{{ $registro->user->name }}</td>
                                    <td>{{ $registro->fecha_historial }}</td>
                                    <td>{{ $registro->estado_actual }}</td>
                                    <td>{{ $registro->estado_anterior }}</td>
                                    <td>{{ $registro->accion }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <script>
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "paging": true,
                "dataSrc": "tableData",
                "oLanguage": {
                    "sLoadingRecords": "Cargando...",
                    "sSearch": "Buscar:",
                    "sZeroRecords": "No se encontraron resultados",
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sEmptyTable": "No se encontraron resultados",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
@stop