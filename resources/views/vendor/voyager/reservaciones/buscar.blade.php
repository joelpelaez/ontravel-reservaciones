@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('page_title',' Buscar Reservación')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-search"></i> Buscar Reservación
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form method="POST" action="{{'resultados'}}" enctype="multipart/form-data" id="formBuscar">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>Campos de búsqueda</h4>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="apellido">Apellido:</label>
                                    <input type="text" name="apellido" id="apellido" class="form-control">
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="id_hotel">Hotel:</label>
                                    <select name="id_hotel" id="id_hotel"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Seleccione un hotel</option>
                                        @foreach($hoteles as $hotel)
                                            <option class="{{$hotel->id_region}}"
                                                    value="{{$hotel->id}}">{{$hotel->nombre}}</option>
                                        @endforeach
                                        <option value="-1">Sin hotel</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="fecha_venta">Fecha de Venta:</label>
                                    <input type="date" name="fecha_venta" id="fecha_venta" value=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="fecha_reservacion">Fecha de Reservación:</label>
                                    <input type="date" name="fecha_reservacion" id="fecha_reservacion" value=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="id_agencia">Agencia:</label>
                                    <select name="id_agencia" id="id_agencia"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Seleccione una agencia</option>
                                        @foreach($agencias as $agencia)
                                            <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                                        @endforeach
                                        <option value="-1">Sin agencia</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2">
                                    <label for="id_rep">Representante:</label>
                                    <select name="id_rep" id="id_rep"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Primero Seleccione una Agencia</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-5 ">
                                    <label for="id_tour">Tour: </label>
                                    <select name="id_tour" id="id_tour"
                                            class="form-control  select2 select2-hidden-accessible">
                                        <option value="">Seleccione un tour</option>
                                        @foreach($tours as $tour)
                                            <option value="{{$tour->id}}">{{$tour->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="id_paquete">Paquete:</label>
                                    <select name="id_paquete" id="id_paquete"
                                            class="form-control  select2 select2-hidden-accessible">
                                        <option value="">Primero Seleccione un Tour</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>Búsqueda por rango - Fecha de Venta:</h4>
                                </div>
                                <div class="form-group col-md-5 ">
                                    <label for="fecha_venta_inicio">Después del: </label>
                                    <input type="date" name="fecha_venta_inicio" id="fecha_venta_inicio" value=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="fecha_venta_fin">Antes del:</label>
                                    <input type="date" name="fecha_venta_fin" id="fecha_venta_fin" value=""
                                           class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>Búsqueda por rango - Fecha de Reservación:</h4>
                                </div>
                                <div class="form-group col-md-5 ">
                                    <label for="fecha_inicio">Después del: </label>
                                    <input type="date" name="fecha_inicio" id="fecha_inicio" value=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-5 col-md-offset-2 ">
                                    <label for="fecha_fin">Antes del:</label>
                                    <input type="date" name="fecha_fin" id="fecha_fin" value="" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" id="btn_buscar" name="btn_buscar"
                                            class="btn btn-success pull-right">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        function getReps() {
            const id = $('#id_agencia option:selected').val();
            $.get('/getreps/' + id, function (data) {
                $('#id_rep').empty();
                $('#id_rep').append('<option value="" disabled="disabled" selected="selected"> Seleccione un representante... </option>');
                $.each(data, function (index, repsObj) {
                    $('#id_rep').append('<option value="' + repsObj.id + '">' + repsObj.nombre + '</option>');
                })
            });
            $('#id_rep').append('<option value="-1">Sin representante</option>');

        }

        function getPaquetesTours() {
            //var id=$('#id_hotel').val();
            const id = $("#id_tour option:selected").val();
            $.get('/getpaquetetours/' + id, function (data) {
                $('#id_paquete').empty();
                $('#id_paquete').append('<option value="" disabled="disabled" selected="selected"> Seleccione un paquete... </option>');
                $.each(data, function (index, pqtoursObj) {
                    $('#id_paquete').append('<option value="' + pqtoursObj.id + '">' + pqtoursObj.nombre + '</option>');
                })
            });
        }

        function getAreaHoteteles() {
            const id = $('#id_hotel option:selected').val();
            $.get('/getareahoteles/' + id, function (data) {
                $('#id_areapickup').empty();
                $('#id_areapickup').append('<option value="" disabled="disabled" selected="selected"> Seleccione un lugar... </option>');
                $.each(data, function (index, areaObj) {
                    $('#id_areapickup').append('<option value="' + areaObj.area_pickup + '">' + areaObj.area_pickup + '</option>');
                })
            });
        }

        function getPickupsHoteles() {
            const area = $('#id_areapickup option:selected').val();
            const id = $('#id_hotel option:selected').val();
            $.get('/getpickupshoteles/' + id + '/' + area, function (data) {
                $('#pickup').empty();
                $('#pickup').append('<option value="" disabled="disabled" selected="selected"> Seleccione un pickup... </option>');
                $.each(data, function (index, pickupObj) {
                    $('#pickup').append('<option value="' + pickupObj.id + '">' + pickupObj.pickup + '</option>');
                })
            });
        }

        function getIdiomasTours() {
            const id = $('#id_tour option:selected').val();
            $.get('/getidiomastours/' + id, function (data) {
                $('#id_idioma').empty();
                $('#id_idioma').append('<option value="" disabled="disabled" selected="selected"> Seleccione un idioma... </option>');
                $.each(data, function (index, idiomaObj) {
                    $('#id_idioma').append('<option value="' + idiomaObj.id + '">' + idiomaObj.nombre + '</option>');
                })
            });
        }

        $("#id_agencia").change(function () {
            getReps();

        });
        $("#id_hotel").change(function () {
            getAreaHoteteles();

        });
        $("#id_tour").change(function () {
            getPaquetesTours();
            getIdiomasTours();
        });
        $("#id_areapickup").change(function () {
            getPickupsHoteles();
        });

        $('#formBuscar #btn_buscar').click(function () {
            if (validate()) $('#formBuscar').submit();
        });

        function validate() {
            $(".errorValid").remove();
            if ($("#formBuscar input").val() === "") {
                $("#id").focus().after("<span class='errorValid'>Por favor ingrese al menos un parametro para buscar</span>");
                return false;
            }

            return true;
        }
    </script>
@stop
