@extends('voyager::master')

@section('page_title',' Resultados de Busqueda')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #777777
        }
    </style>

@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-pie-chart"></i> Resultados de la orden de servicio
    </h1>
    <a type="button" class="btn btn-sm btn-info" target="blank" title="Regresar"
       href="{{'excel'}}/?fecha_reservacion={{$fecha_reservacion}}&id_tour={{$id_tour}}">
        <i class="voyager-cloud-download"></i> <span class="hidden-xs hidden-sm">Descargar Excel</span>
    </a>
    <a type="button" class="btn btn-sm btn-dark" title="Regresar" href="buscar">
        <i class="voyager-double-left"></i> <span class="hidden-xs hidden-sm">Regresar</span>
    </a>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content  container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive" style="overflow-x:auto;">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Apellido</th>
                                    <th>Idioma</th>
                                    <th>PAX</th>
                                    <th>Cupón</th>
                                    <th>Origen</th>
                                    <th>Habitación</th>
                                    <th>Pickup</th>
                                    <th>Agencia</th>
                                    <th>Representante</th>
                                    <th>Observaciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_pax = '';
                                $total_adultos = 0;
                                $total_ninos = 0;
                                ?>
                                @if (count($reservas) > 0)
                                    @foreach($reservas as $reserva)
                                        @foreach($reserva->cupon as $cupon)
                                            <?php
                                            $total_adultos += $cupon->n_adultos;
                                            $total_ninos += $cupon->n_ninos;
                                            ?>
                                            <tr>
                                                <td>
                                                    {{$reserva->fecha_reservacion->format('d/m/Y')}}
                                                </td>
                                                <td>
                                                    {{$reserva->apellido}}
                                                </td>
                                                <td>
                                                    {{$reserva->idiomaTour->idioma->nombre}}
                                                </td>
                                                <td>
                                                    @if($cupon->n_ninos > 0)
                                                        {{ $cupon->n_adultos . '.' . $cupon->n_ninos }}
                                                    @else
                                                        {{ $cupon->n_adultos }}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $cupon->cupon }}
                                                </td>
                                                <td>
                                                    @if($reserva->hotel !== null)
                                                        {{ $reserva->hotel->nombre }}
                                                    @else
                                                        {{ $reserva->direccion }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reserva->habitacion !== null)
                                                        {{ $reserva->habitacion }}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reserva->pickup !== null)
                                                        {{ $reserva->pickup->pickup }}
                                                    @else
                                                        {{ date('H:i', strtotime($reserva->pickup_hora)) }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reserva->agencia !== null)
                                                        {{ $reserva->agencia->nombre }}
                                                    @else
                                                        Sin Agencia
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reserva->representante !== null)
                                                        {{ $reserva->representante->nombre }}
                                                    @else
                                                        Personal
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$cupon->observaciones}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "paging": true,
                "dataSrc": "tableData",
                "oLanguage": {
                    "sLoadingRecords": "Cargando...",
                    "sSearch": "Buscar:",
                    "sZeroRecords": "No se encontraron resultados",
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sEmptyTable": "No se encontraron resultados",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                initComplete: function () {
                    this.api().column(3).each(function () {
                        var column = this;
                        //console.log(column.data());
                        $('input[name$="name"]').on('click change', function () {
                            var val = $(this).val();
                            column.search(val).draw();

                        });

                    });
                }
            });


        });

        $('body').on('click', '.cupon', function (e) {

            var myButtons = $("#dataTable [name='boton_cupones[]'");
            var index_button = myButtons.index(this);
            var id = $(".cupon:eq(" + index_button + ")").val();
            $.get('/getcuponesbyidreservacion/' + id, function (data) {
                //$('#id_paquete').empty();
                //$('#id_paquete').append('<option value="" disable="true" selected="true"> Seleccione un paquete... </option>');
                $.each(data, function (index, cuponObj) {
                    $(".cupon").append('<tr  class="panel_oculto" style="display:none; background-color:#DDDDDD;" ><td></td><td>Cupon: ' + cuponObj.cupon + '</td><td>#Adultos: ' + cuponObj.n_adultos +
                        '</td><td >#Niños: ' + cuponObj.n_ninos + '</td><td>Tipo de Cupon:' + cuponObj.tipoCupon.nombre + '</td>' +
                        '<td>Status:' + cuponObj.statusCupon.nombre + '</td><td colspan="3">Observaciones:' + cuponObj.observaciones + '</td></tr> ');
                })
            });


            var trOculto = $(".panel_oculto:eq(" + index_button + ")");

            trOculto.toggle();
        });

        $('#myModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formReservacion"]').attr('action', id + "/{{'cancelar'}}");

            $(e.currentTarget).find('input[id="btnEnviar"]').click(function (event) {
                $('#formReservacion').submit();
            });

        });

        $('#modal_historial_reservacion').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formReservacion"]').attr('action', id + "/{{'cancelar'}}");

            $(e.currentTarget).find('input[id="btnEnviar"]').click(function (event) {
                $('#formReservacion').submit();
            });

        });
    </script>

@stop
