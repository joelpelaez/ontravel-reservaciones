@extends('voyager::master')

@section('page_title',' Resultados de Busqueda')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #777777
        }
    </style>

@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-pie-chart"></i> Resultados del Bueno Para Cobro
    </h1>
    <a type="button" class="btn btn-sm btn-dark" title="Regresar" href="buscar">
        <i class="voyager-double-left"></i> <span class="hidden-xs hidden-sm">Regresar</span>
    </a>
    <form style="display: inline;" method="post" target="_blank" enctype="multipart/form-data"
          action="{{ route('buenoparacobro.excel') }}">
        {{ csrf_field() }}
        <input type="hidden" name="agencia_id" value="{{ $agencia_id }}">
        <input type="hidden" name="tour_id" value="{{ $tour_id }}">
        <input type="hidden" name="fecha_inicio" value="{{ $fecha_inicio }}">
        <input type="hidden" name="fecha_fin" value="{{ $fecha_fin }}">
        <input type="submit" value="Exportar a Excel" class="btn btn-sm btn-primary">
    </form>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content  container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive" style="overflow-x:auto;">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Confirmación</th>
                                    <th>Tour</th>
                                    <th>Paquete</th>
                                    <th>Agencia</th>
                                    <th>Representante</th>
                                    <th>Hotel</th>
                                    <th>Apellido</th>
                                    <th>Habitación</th>
                                    <th>Fecha Reservacion</th>
                                    <th>Fecha Venta</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($reservas) > 0)
                                    @foreach($reservas as $reserva)
                                        <tr>
                                            <td>
                                                {{$reserva->id}}
                                            </td>
                                            <td>
                                                {{$reserva->confirmacion}}
                                            </td>
                                            <td>
                                                {{$reserva->paqueteTour->tour->nombre}}
                                            </td>
                                            <td>
                                                {{$reserva->paqueteTour->paquete->nombre}}
                                            </td>
                                            <td>
                                                @if($reserva->agencia !== null)
                                                    {{ $reserva->agencia->nombre }}
                                                @else
                                                    Sin Agencia
                                                @endif
                                            </td>
                                            <td>
                                                @if($reserva->representante !== null)
                                                    {{ $reserva->representante->nombre }}
                                                @else
                                                    Personal
                                                @endif
                                            </td>
                                            <td>
                                                @if($reserva->hotel !== null)
                                                    {{ $reserva->hotel->nombre }}
                                                @else
                                                    {{ $reserva->direccion }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ $reserva->apellido }}
                                            </td>
                                            <td>
                                                @if($reserva->habitacion !== null)
                                                    {{ $reserva->habitacion }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                {{ $reserva->fecha_reservacion->format('Y/m/d') }}
                                            </td>
                                            <td>
                                                {{ $reserva->fecha_venta->format('Y/m/d') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "paging": true,
                "dataSrc": "tableData",
                "oLanguage": {
                    "sLoadingRecords": "Cargando...",
                    "sSearch": "Buscar:",
                    "sZeroRecords": "No se encontraron resultados",
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sEmptyTable": "No se encontraron resultados",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                initComplete: function () {
                    this.api().column(3).each(function () {
                        var column = this;
                        //console.log(column.data());
                        $('input[name$="name"]').on('click change', function () {
                            var val = $(this).val();
                            column.search(val).draw();

                        });

                    });
                }
            });


        });

        $('body').on('click', '.cupon', function (e) {

            var myButtons = $("#dataTable [name='boton_cupones[]'");
            var index_button = myButtons.index(this);
            var id = $(".cupon:eq(" + index_button + ")").val();
            $.get('/getcuponesbyidreservacion/' + id, function (data) {
                //$('#id_paquete').empty();
                //$('#id_paquete').append('<option value="" disable="true" selected="true"> Seleccione un paquete... </option>');
                $.each(data, function (index, cuponObj) {
                    $(".cupon").append('<tr  class="panel_oculto" style="display:none; background-color:#DDDDDD;" ><td></td><td>Cupon: ' + cuponObj.cupon + '</td><td>#Adultos: ' + cuponObj.n_adultos +
                        '</td><td >#Niños: ' + cuponObj.n_ninos + '</td><td>Tipo de Cupon:' + cuponObj.tipoCupon.nombre + '</td>' +
                        '<td>Status:' + cuponObj.statusCupon.nombre + '</td><td colspan="3">Observaciones:' + cuponObj.observaciones + '</td></tr> ');
                })
            });


            var trOculto = $(".panel_oculto:eq(" + index_button + ")");

            trOculto.toggle();
        });

        $('#myModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formReservacion"]').attr('action', id + "/{{'cancelar'}}");

            $(e.currentTarget).find('input[id="btnEnviar"]').click(function (event) {
                $('#formReservacion').submit();
            });

        });

        $('#modal_historial_reservacion').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            $(e.currentTarget).find('form[id="formReservacion"]').attr('action', id + "/{{'cancelar'}}");

            $(e.currentTarget).find('input[id="btnEnviar"]').click(function (event) {
                $('#formReservacion').submit();
            });

        });
    </script>

@stop
