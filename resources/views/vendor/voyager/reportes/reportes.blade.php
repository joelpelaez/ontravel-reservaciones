 @extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('page_title',' Reportes')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-pie-chart"></i> Reportes
    </h1>
 @include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content container-fluid">
    <div class="row">
            <div class="panel panel-bordered col-lg-3 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading"><h3>Orden de Servicio</h3></div>
                        <div class="panel-body">
                          <a href="reportes/ordendeservicio/buscar" class="small-box-footer"><i class="icon voyager-search">Buscar</i></a>
         
                        </div>
                    </div>
            </div>
            <div class="panel panel-bordered col-lg-3 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading"><h3>Bueno para cobro</h3></div>
                        <div class="panel-body">
                          <a href="reportes/buenoparacobro/buscar" class="small-box-footer"><i class="icon voyager-search">Buscar</i></a>
         
                        </div>
                    </div>
            </div>
    </div>
</div>
@stop