@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('page_title','Exportar a Excel - Error')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-pie-chart"></i>Exportación a Excel ha fallado
    </h1>
    <a type="button" class="btn btn-sm btn-dark"  title="Regresar" href="../">
        <i class="voyager-double-left"></i><span class="hidden-xs hidden-sm">Regresar</span>
    </a>
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <section>
                    <p>Ha ocurrido un error al momento de crear el archivo Excel, revise si la búsqueda o consulta
                    genera más de un resultado e intente de nuevo. Si vuelve a fallar, reporte la incidencia.</p>
                </section>
            </div>
        </div>
    </div>
@stop

