@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('page_title',' Bueno para cobro')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-pie-chart"></i> Reporte
    </h1>
    <a type="button" class="btn btn-sm btn-dark" title="Regresar" href="../../reportes">
        <i class="voyager-double-left"></i> <span class="hidden-xs hidden-sm">Regresar</span>
    </a>
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-8">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- form start -->
                <form method="POST" action="{{'resultados'}}" enctype="multipart/form-data" id="formBuscar">
                    {{ csrf_field() }}
                    <div class="panel panel-bordered">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3>Bueno para cobro</h3>
                            </div>
                            <div class="panel-body">
                                <h4>Rango de Fechas</h4>
                                <div class="form-group col-md-5 ">
                                    <input type="date" name="fecha_inicio" id="fecha_inicio" value=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-md-5">
                                    <input type="date" name="fecha_fin" id="fecha_fin" value="" class="form-control">
                                </div>
                                <div class="form-group col-md-5 ">
                                    <label for="Tour">Tour: </label>
                                    <select name="id_tour" id="id_tour"
                                            class="form-control  select2 select2-hidden-accessible">
                                        <option value="">Seleccione un tour</option>
                                        @foreach($tours as $tour)
                                            <option value="{{$tour->id}}">{{$tour->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="Agencia">Agencia:</label>
                                    <select name="id_agencia" id="id_agencia"
                                            class="form-control select2 select2-hidden-accessible">
                                        <option value="">Seleccione una agencia</option>
                                        @foreach($agencias as $agencia)
                                            <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="button" id="btn_buscar" name="btn_buscar"
                                        class="btn btn-success pull-right">
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('#formBuscar #btn_buscar').click(function () {
            if (validate()) $('#formBuscar').submit();
        });

        function validate() {
            $(".errorValid").remove();
            if ($("#formBuscar input").val() === "") {
                $("#id").focus().after("<span class='errorValid'>Por favor ingrese al menos un parametro para buscar</span>");
                return false;
            }

            return true;
        }
    </script>

@stop
