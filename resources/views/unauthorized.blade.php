@extends('voyager::master')

@section('page_title', 'Acceso restringido')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        Acceso restringido
    </h1>
@stop

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <p>
                            No tiene acceso a esta sección del sistema, regrese a la página anterior.
                            <a href="#" onclick="goBack()">Regresar</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@stop